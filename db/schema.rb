# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140616075118) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "active_admin_comments", force: true do |t|
    t.string   "namespace"
    t.text     "body"
    t.string   "resource_id",   null: false
    t.string   "resource_type", null: false
    t.integer  "author_id"
    t.string   "author_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "active_admin_comments", ["author_type", "author_id"], name: "index_active_admin_comments_on_author_type_and_author_id", using: :btree
  add_index "active_admin_comments", ["namespace"], name: "index_active_admin_comments_on_namespace", using: :btree
  add_index "active_admin_comments", ["resource_type", "resource_id"], name: "index_active_admin_comments_on_resource_type_and_resource_id", using: :btree

  create_table "admin_users", force: true do |t|
    t.string   "email",                  default: "",    null: false
    t.string   "encrypted_password",     default: "",    null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,     null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "super_admin",            default: false, null: false
    t.boolean  "blocked",                default: false, null: false
  end

  add_index "admin_users", ["email"], name: "index_admin_users_on_email", unique: true, using: :btree
  add_index "admin_users", ["reset_password_token"], name: "index_admin_users_on_reset_password_token", unique: true, using: :btree

  create_table "attributes", force: true do |t|
    t.string  "name",    null: false
    t.integer "unit_id"
  end

  add_index "attributes", ["name"], name: "index_attributes_on_name", unique: true, using: :btree

  create_table "attributes_categories", id: false, force: true do |t|
    t.integer "category_id"
    t.integer "attribute_id"
  end

  add_index "attributes_categories", ["attribute_id"], name: "index_attributes_categories_on_attribute_id", using: :btree
  add_index "attributes_categories", ["category_id"], name: "index_attributes_categories_on_category_id", using: :btree

  create_table "cart_items", force: true do |t|
    t.integer "entity_id"
    t.integer "cart_id"
    t.integer "quantity",  default: 1
  end

  add_index "cart_items", ["cart_id"], name: "index_cart_items_on_cart_id", using: :btree
  add_index "cart_items", ["entity_id"], name: "index_cart_items_on_entity_id", using: :btree

  create_table "carts", force: true do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "user_id"
    t.integer  "admin_user_id"
    t.boolean  "user_confirmation",       default: false
    t.boolean  "admin_user_confirmation", default: false
    t.string   "cart_info"
  end

  add_index "carts", ["admin_user_id"], name: "index_carts_on_admin_user_id", using: :btree
  add_index "carts", ["user_id"], name: "index_carts_on_user_id", using: :btree

  create_table "categories", force: true do |t|
    t.string   "title"
    t.text     "description"
    t.boolean  "active"
    t.integer  "parent_id",      default: 0, null: false
    t.integer  "order"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "image_category"
  end

  add_index "categories", ["title"], name: "index_categories_on_title", unique: true, using: :btree

  create_table "currencies", force: true do |t|
    t.string   "name"
    t.string   "abbreviation"
    t.decimal  "ratio",            precision: 5, scale: 2
    t.string   "emblem"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "display"
    t.boolean  "prime_descriptor",                         default: false
    t.boolean  "sec_descriptor",                           default: false
  end

  add_index "currencies", ["name"], name: "index_currencies_on_name", unique: true, using: :btree

  create_table "entities", force: true do |t|
    t.string   "title"
    t.decimal  "price",           precision: 5, scale: 2
    t.boolean  "price_exchange",                          default: false, null: false
    t.integer  "currency_id"
    t.text     "description"
    t.integer  "discount_id"
    t.string   "cover"
    t.boolean  "published",                               default: true,  null: false
    t.integer  "views"
    t.text     "characteristics"
    t.integer  "manufacturer_id"
    t.integer  "category_id"
    t.integer  "availability"
    t.integer  "guarantee"
    t.string   "advise"
    t.date     "price_end_date"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "entities", ["category_id"], name: "index_entities_on_category_id", using: :btree
  add_index "entities", ["currency_id"], name: "index_entities_on_currency_id", using: :btree
  add_index "entities", ["discount_id"], name: "index_entities_on_discount_id", using: :btree
  add_index "entities", ["manufacturer_id"], name: "index_entities_on_manufacturer_id", using: :btree
  add_index "entities", ["title"], name: "index_entities_on_title", unique: true, using: :btree

  create_table "entities_features", id: false, force: true do |t|
    t.integer "entity_id"
    t.integer "feature_id"
  end

  add_index "entities_features", ["entity_id"], name: "index_entities_features_on_entity_id", using: :btree
  add_index "entities_features", ["feature_id"], name: "index_entities_features_on_feature_id", using: :btree

  create_table "features", force: true do |t|
    t.string   "name"
    t.text     "description"
    t.string   "label"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "manufacturers", force: true do |t|
    t.string   "name"
    t.string   "thumb"
    t.text     "description"
    t.string   "url"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "parameters", force: true do |t|
    t.integer "attribute_id"
    t.integer "entity_id"
    t.string  "value"
  end

  add_index "parameters", ["attribute_id"], name: "index_parameters_on_attribute_id", using: :btree
  add_index "parameters", ["entity_id"], name: "index_parameters_on_entity_id", using: :btree

  create_table "profiles", force: true do |t|
    t.boolean "email_visible", default: false, null: false
    t.date    "birthday"
    t.string  "address"
    t.boolean "gender"
    t.text    "about"
    t.string  "signature"
    t.string  "phone"
    t.string  "skype"
    t.string  "url"
    t.string  "time_zone"
    t.string  "avatar"
    t.integer "user_id"
    t.string  "username"
  end

  create_table "units", force: true do |t|
    t.string "param"
  end

  create_table "users", force: true do |t|
    t.string   "name",                   default: "", null: false
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["name"], name: "index_users_on_name", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

end
