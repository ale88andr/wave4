class CreateEntitiesFeatures < ActiveRecord::Migration
  def change
    create_table :entities_features, id: false do |t|
      t.references :entity, 	index: true
      t.references :feature, 	index: true
    end
  end
end
