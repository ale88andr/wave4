class CreateCategories < ActiveRecord::Migration
  def change
    create_table :categories do |t|
      t.string  :title
      t.text    :description
      t.boolean :active
      t.integer :parent_id,   default: 0, null: false
      t.integer :order

      t.timestamps
    end
    add_index :categories, :title, unique: true
  end
end
