class CreateCurrencies < ActiveRecord::Migration
  def change
    create_table :currencies do |t|
      t.string 	:name
      t.string 	:abbreviation
      t.decimal	:ratio,      		precision: 5, scale: 2
      t.string 	:emblem

      t.timestamps
    end

    add_index :currencies, :name, :unique => true
  end
end
