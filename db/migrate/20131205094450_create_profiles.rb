class CreateProfiles < ActiveRecord::Migration
  def change
    create_table :profiles do |t|
      t.boolean     :email_visible, :null => false, :default => false
      t.date        :birthday
      t.string      :address
      t.boolean     :gender
      t.text        :about
      t.string      :signature
      t.string      :phone
      t.string      :skype
      t.string      :url
      t.string      :time_zone
      t.string      :avatar
      t.references  :user
      t.string      :username
    end
  end
end
