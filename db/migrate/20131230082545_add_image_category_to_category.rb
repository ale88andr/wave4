class AddImageCategoryToCategory < ActiveRecord::Migration
  def change
    add_column :categories, :image_category, :string
  end
end
