class CreateAttributes < ActiveRecord::Migration
  def change
    create_table :attributes do |t|
      t.string :name, 		null: false
      t.references :unit
    end

    add_index :attributes, :name, :unique => true
  end
end
