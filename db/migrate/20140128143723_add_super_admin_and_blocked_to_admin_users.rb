class AddSuperAdminAndBlockedToAdminUsers < ActiveRecord::Migration
  def change
    add_column :admin_users, :super_admin, 	:boolean, null: false, default: false
    add_column :admin_users, :blocked, 			:boolean, null: false, default: false
  end
end
