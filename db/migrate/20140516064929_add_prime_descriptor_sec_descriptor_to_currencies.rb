class AddPrimeDescriptorSecDescriptorToCurrencies < ActiveRecord::Migration
  def change
    add_column :currencies, :prime_descriptor, :boolean, default: false
    add_column :currencies, :sec_descriptor, :boolean, default: false
  end
end
