class AddFieldsToCart < ActiveRecord::Migration
  def change
    add_reference :carts, :user, index: true
    add_reference :carts, :admin_user, index: true
    add_column 		:carts, :user_confirmation, :boolean, default: false
    add_column 		:carts, :admin_user_confirmation, :boolean, default: false
    add_column 		:carts, :cart_info, :string
  end
end
