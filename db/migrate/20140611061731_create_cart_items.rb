class CreateCartItems < ActiveRecord::Migration
  def change
    create_table :cart_items do |t|
      t.references :entity, index: true
      t.belongs_to :cart, index: true
    end
  end
end
