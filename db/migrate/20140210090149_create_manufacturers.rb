class CreateManufacturers < ActiveRecord::Migration
  def change
    create_table :manufacturers do |t|
      t.string 	:name
      t.string 	:thumb
      t.text 		:description
      t.string 	:url

      t.timestamps
    end
  end
end
