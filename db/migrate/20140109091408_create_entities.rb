class CreateEntities < ActiveRecord::Migration
  def change
    create_table :entities do |t|
      t.string      :title
      t.decimal     :price,           precision: 5, scale: 2
      t.boolean     :price_exchange,  null: false, default: false
      t.references  :currency
      t.text        :description
      t.references  :discount
      t.string      :cover
      t.boolean     :published,       null: false, default: true
      t.integer     :views
      t.text        :characteristics
      t.references  :manufacturer
      t.references  :category
      t.integer     :availability
      t.integer     :guarantee
      t.string      :advise
      t.date        :price_end_date

      t.timestamps
    end

    add_index :entities, :title,           :unique => true
    add_index :entities, :currency_id
    add_index :entities, :discount_id
    add_index :entities, :manufacturer_id
    add_index :entities, :category_id

  end
end
