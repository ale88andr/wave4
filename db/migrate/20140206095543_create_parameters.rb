class CreateParameters < ActiveRecord::Migration
  def change
    create_table :parameters do |t|
      t.references 	:attribute, index: true
      t.references 	:entity, 		index: true
      t.string 			:value
    end
  end
end
