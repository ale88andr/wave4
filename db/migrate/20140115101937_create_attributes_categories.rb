class CreateAttributesCategories < ActiveRecord::Migration
  def change
    create_table :attributes_categories, id: false do |t|
      t.belongs_to :category, index: true
      t.belongs_to :attribute, index: true
    end
  end
end
