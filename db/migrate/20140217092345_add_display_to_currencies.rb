class AddDisplayToCurrencies < ActiveRecord::Migration
  def change
    add_column :currencies, :display, :boolean
  end
end
