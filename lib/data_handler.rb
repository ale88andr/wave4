module DataHandler
  # unsafe_str - also, input string from user form
  def safe_str unsafe_str
    unsafe_str.squish.downcase.titleize
  end

  # unsafe_txt - input text from user form, truncate_length - text truncate length
  def safe_txt unsafe_txt, truncate_length = 65000
    unsafe_txt.squish.capitalize.truncate(truncate_length, omission: ' ') unless unsafe_txt.blank?
  end
end