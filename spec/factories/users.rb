# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :user do
    name                  { Faker::Name.name }
    email                 { Faker::Internet.email }
    password              'Pa$$w0rd'
    password_confirmation 'Pa$$w0rd'

    factory :user_with_profile do
      after :create do |user|
        create :profile, user: user
      end
    end
  end

  factory :admin_user do
    email     { Faker::Internet.email }
    password  'Pa$$w0rd'
  end
end
