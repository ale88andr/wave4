# Read about factories at https://github.com/thoughtbot/factory_girl
FactoryGirl.define do
  factory :cart do
    factory :cart_with_items do
      after(:create) do |cart|
        cart.cart_items << create(:cart_item)
      end
    end

    factory :cart_confirm do
      cart_info {{  username: Faker::Name.name,
                    city:     Faker::Address.city,
                    phone:    Faker::PhoneNumber.phone_number,
                    note:     Faker::Lorem.word,
                    street:   Faker::Address.street_address
                }}
    end

    trait :confirmed do
      user_confirmation       true
      admin_user_confirmation true
    end

    trait :with_customer_info do
      cart_info {{  username: Faker::Name.name,
                    city:     Faker::Address.city,
                    phone:    Faker::PhoneNumber.phone_number,
                    note:     Faker::Lorem.word,
                    street:   Faker::Address.street_address
                }}
    end

    factory :full_cart, parent: :cart_with_items, traits: [:with_customer_info, :confirmed]
  end
end
