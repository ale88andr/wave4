# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :manufacturer do
    name          { Faker::Product.brand }
    thumb         'path/to/thumb'
    description   { Faker::Lorem.sentence }
    url           { Faker::Internet.http_url }
  end
end
