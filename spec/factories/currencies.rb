# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :currency do
    name              Currency::TYPES[:RUB]
    ratio             { rand(0..999) }
    display           true
    prime_descriptor  false
    sec_descriptor    false
    abbreviation      { Faker::Lorem.word }

    trait :prime do
      prime_descriptor  true
    end

    trait :secondary do
      sec_descriptor    true
    end

    factory :prime_currency,  traits: [:prime]
    factory :sec_currency,    traits: [:secondary]
  end
end
