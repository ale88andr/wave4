FactoryGirl.define do
  factory :profile do
    username      Faker::Name.name
    url           Faker::Internet.http_url
    phone         Faker::PhoneNumber.phone_number
    skype         'Skype'
    birthday      Faker::Time.date
    email_visible true
    gender        1
    address       {{street: Faker::Address.street_address, city: Faker::Address.city}}
    # about         Faker::Lorem.sentences(2)
    time_zone     'Kyiv'
    avatar        'path/to/avatar'
    user
  end
end