# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :cart_item do
    association :entity,  factory: :entity_for_cart
  end
end
