FactoryGirl.define do
  factory :category do
    title         { Faker::Product.brand}
    description   { Faker::Lorem.paragraph }
    parent_id     0
    active        true

    factory :category_with_attributes do
      after(:create) do |category|
        category.property_ids << create_list(:attribute, 3)
      end
    end
  end

  trait :inactive do
    active  false
  end

  trait :created_month_ago do
    created_at  { 1.month.ago }
  end

  trait :created_week_ago do
    created_at  { 1.week.ago }
  end

  trait :created_day_ago do
    created_at  { 1.day.ago }
  end

  factory :category_with_entities, parent: :category do
    entities { create_list :entity, 3 }
  end

  factory :created_day_ago_category,    traits: [:created_day_ago]
  factory :created_week_ago_category,   traits: [:created_week_ago]
  factory :created_month_ago_category,  traits: [:created_month_ago]
end
