# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :parameter, :class => 'Parameters' do
    attribute nil
    entity nil
    value "MyString"
  end
end
