# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :attribute, class: 'Attribute' do
    name      { Faker::Product.brand }
    unit_id   1
  end
end
