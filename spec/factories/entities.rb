# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :entity do
    title           { Faker::Product.product }
    price           { rand(0..999) }
    price_exchange  false
    description     { Faker::Lorem.paragraph }
    # discount
    cover           "PATH/TO/IMAGE"
    published       false
    views           { rand(0..999) }
    characteristics { Faker::Lorem.paragraph }
    availability    { rand(0..999) }
    guarantee       { rand(0..9) }
    # advise          { Faker::Lorem.word }

    # association :manufacturer, factory: :manufacturer

    after(:create) do |entity|
      entity.feature_ids << create_list(:feature, 3)
    end

    trait :with_category do
      association :category,     factory: :category_with_attributes
    end

    trait :with_currency do
      association :currency,     factory: :prime_currency 
    end

    factory :entity_for_cart, parent: :entity, traits: [:with_category, :with_currency]
  end
end
