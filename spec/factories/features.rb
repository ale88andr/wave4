# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :feature do
    name        { Faker::Name.name }
    description { Faker::Lorem.paragraph }
    label       'path/to/label'
  end
end
