require 'spec_helper'

describe Store::Orders::CartsController do
  describe 'GET #show' do
    context 'with cart items' do
      let!(:cart) { create :cart_with_items }

      before { get :show, id: cart }

      it "assigns the requested cart to @cart" do
        expect(assigns(:cart)).to eq cart
      end

      it "renders the :show template" do
        expect(response).to render_template :show
      end
    end

    context 'with empty cart' do
      let!(:cart) { create :cart }

      before { get :show, id: cart }

      it "renders the :empty template" do
        expect(response).to render_template :empty
      end
    end
  end

  describe 'GET #empty' do
    before { get :empty }

    it "renders the :empty template" do
      expect(response).to render_template :empty
    end
  end

  describe 'GET #confirmation' do
    let!(:cart) { create :cart }

    before { get :confirmation, id: cart }

    it "renders the :new template" do
      expect(response).to render_template :confirmation
    end

    it "assigns the requested cart to @cart" do
      expect(assigns(:cart)).to eq cart
    end
  end

  describe 'PUT #create_confirmation' do
    context 'with valid attributes' do
      let(:cart) { create :cart_confirm }

      before { put :create_confirmation, id: cart, cart: cart.cart_info }

      it "updates cart record in the database" do
        expect(Cart.find(cart.id)).to eq cart
      end

      it "unset session cart_id" do
        expect(session[:cart_id]).to be_nil
      end

      it "set user_confirm to true" do
        expect(Cart.find(cart.id).user_confirmation).to be
      end

      it "redirects to the greeting" do
        expect(response).to redirect_to root_url
      end
    end

    context 'with invalid attributes' do
      it "does not update the cart record"
      it "leave session cart_id"
      it "re-renders #confirmation"
      it "ons an error message"
    end
  end

  describe 'DELETE #destroy' do
    context 'on existing cart' do
      let!(:cart) { create :cart_with_items }

      before { session[:cart_id] = cart.id }

      it "delete @cart record from database" do
        expect{
          delete :destroy, id: cart.id
        }.to change(Cart, :count).by(-1)
      end

      it "delete @cart.cart_items records" do
        delete :destroy, id: cart.id
        expect(cart.cart_items.reload.present?).to be_false
      end

      it "must be redirected to root" do
        delete :destroy, id: cart.id
        expect(response).to redirect_to root_url
      end
    end
  end
end
