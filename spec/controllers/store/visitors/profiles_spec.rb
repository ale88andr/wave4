require 'spec_helper'

describe Store::Visitors::ProfilesController do

  let!(:user) { create(:user) }

  before { sign_in user }

  describe 'GET #new' do

    before { get :new, user_id: user }

    it "assigns a new Contact to @contact" do
      expect(assigns(:profile)).to be_a_new(Profile)
    end

    it "renders the :new template" do
      expect(response).to render_template :new
    end
  end

  describe 'POST #create' do
    context 'with valid attributes' do
      it 'saves new profile in the database' do
        expect{ post :create, user_id: user, profile: attributes_for(:profile) }.
              to change(Profile, :count).
              by(1)
      end

      it 'redirect to the root page' do
        post :create, user_id: user, profile: attributes_for(:profile)
        expect(response).to redirect_to root_path
      end
    end

    context 'with invalid attributes' do
      it 'not saves invalid profile in the database' do
        expect{ post :create, user_id: user, profile: attributes_for(:profile, username: 'a' * 26) }.
              not_to change(Profile, :count)
      end

      it 'redirect to the profile form' do
        post :create, user_id: user, profile: attributes_for(:profile, username: nil)
        expect(response).to render_template :new
      end
    end
  end
end
