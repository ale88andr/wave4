require 'spec_helper'

describe Store::Visitors::UsersController do

  before { sign_in user }

  describe 'GET #show' do
    let(:user)    { create(:user) }
    let(:profile) { create(:profile, user: user) }

    before { get :show, id: user }

    it 'assigns the requested profile to @profile' do
      expect(assigns(:profile)).to eq(user.profile)
    end

    it 'render :show template' do
      expect(response).to render_template :show
    end
  end
end