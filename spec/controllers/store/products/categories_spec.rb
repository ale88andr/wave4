require 'spec_helper'

describe Store::Products::CategoriesController do
  describe 'GET #index' do
    let(:categories) { create_list :category, 3 }

    before { get :index }

    it "populates an array of categories" do
      expect(assigns(:categories)).to match_array categories
    end

    it "renders the :index template" do
      expect(response).to render_template :index
    end
  end

  describe 'GET #show' do
    context 'on exists category' do
      let(:category) { create :category }

      before { get :show, id: category.id }

      it "assigns the requested category to @category" do
        expect(assigns(:category_decorator)).to eq category
      end

      it "renders the :show template" do
        expect(response).to render_template :show
      end
    end

    context 'on non exists category' do  
      it "should raise exeption" do
        expect{ get :show, id: 1001 }.to raise_error ActiveRecord::RecordNotFound
      end

      it "renders 200 status code" do
        pending #expect{ get :show, id: 1001 }.to eq(200)
      end
    end
  end
end