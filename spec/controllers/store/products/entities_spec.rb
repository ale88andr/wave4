require 'spec_helper'

describe Store::Products::EntitiesController do

  describe 'GET #show' do

    let(:category) { create :category }

    context 'on exists entity' do

      let(:entity) { create :entity }

      before { get :show, category_id: category, id: entity.id }

      it "assigns the requested entity to @presenter" do
        expect(assigns(:presenter)).to eq entity
      end

      it "renders the :show template" do
        expect(response).to render_template :show
      end

      it "increment views property" do
        expect(Entity.find(entity.id).views).to eq entity.views.next
      end
    end

    context 'on non exists entity' do  
      it "should raise exeption" do
        expect{ get :show, category_id: category, id: 1001 }.to raise_error ActiveRecord::RecordNotFound
      end
    end
  end

  describe 'GET #index' do
    pending
  end
end
