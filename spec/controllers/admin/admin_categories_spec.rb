require 'spec_helper'

describe Admin::CategoriesController do

  before { sign_in(FactoryGirl.create :admin_user) }

  let(:resource_class) { Category }
  let(:all_resources) { ActiveAdmin.application.namespaces[:admin].resources }
  let(:resource) { all_resources[resource_class] }

  it { expect(resource.resource_name).to eq "Category" }
    
  describe 'menu' do
    it { expect(resource).to be_include_in_menu }
    it { expect(resource.menu_item_options[:label]).to eq "Категории товаров" }
  end

  describe 'actions' do
    it { expect(resource.defined_actions).to match_array [:create, :new, :update, :edit, :index, :show, :destroy] }

    context 'GET #index' do
      subject { get :index }
      it_should_behave_like "default action behavior", :index
    end

    context 'GET #show' do
      subject { get :show, id: create(:category) }
      it_should_behave_like "default action behavior", :show
    end

    context 'GET #new' do
      subject { get :new }
      it_should_behave_like "default action behavior", :new
    end

    context 'GET #edit' do
      subject { get :edit, id: create(:category) }
      it_should_behave_like "default action behavior", :edit
    end
  end
end