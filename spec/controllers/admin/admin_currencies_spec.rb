require 'spec_helper'

describe Admin::CurrenciesController do

  before { sign_in(FactoryGirl.create :admin_user) }

  let(:resource_class) { Currency }
  let(:all_resources) { ActiveAdmin.application.namespaces[:admin].resources }
  let(:resource) { all_resources[resource_class] }

  it { expect(resource.resource_name).to eq "Currency" }
    
  describe 'menu' do
    it { expect(resource).to be_include_in_menu }
    it { expect(resource.menu_item_options[:label]).to eq "Валюта" }
  end

  describe 'actions' do
    it { expect(resource.defined_actions).not_to include [:show] }

    context 'GET #index' do
      subject { get :index }
      it_should_behave_like "default action behavior", :index
    end

    context 'GET #new' do
      subject { get :new }
      it_should_behave_like "default action behavior", :new
    end
  end
end