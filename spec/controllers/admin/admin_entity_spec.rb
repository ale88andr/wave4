require 'spec_helper'

describe Admin::EntitiesController do

  before { sign_in(FactoryGirl.create :admin_user) }

  let(:resource_class) { Entity }
  let(:all_resources) { ActiveAdmin.application.namespaces[:admin].resources }
  let(:resource) { all_resources[resource_class] }

  it { expect(resource.resource_name).to eq "Entity" }
    
  describe 'menu' do
    it { expect(resource).to be_include_in_menu }
    it { expect(resource.menu_item_options[:label]).to eq "Товары" }
  end

  describe 'actions' do

    let!(:category) { create :category }

    it { expect(resource.defined_actions).to match_array [:create, :new, :update, :edit, :index, :show, :destroy] }

    context 'GET#new' do
      subject { get :new, category_id: category }
      it { expect(subject).to be_success }
    end

    context 'GET #index' do
      subject { get :index, category_id: category }
      it_should_behave_like "default action behavior", :index
    end

    context 'GET #show' do

      subject { get :show, category_id: category, id: create(:entity) }

      it_should_behave_like "default action behavior", :show
      it { expect(subject).to render_template :show_details }
    end
  end
end