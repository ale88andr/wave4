require 'spec_helper'

describe Admin::CartsController do

  before { sign_in(create :admin_user) }

  let(:resource_class) { Cart }
  let(:all_resources) { ActiveAdmin.application.namespaces[:admin].resources }
  let(:resource) { all_resources[resource_class] }

  it { expect(resource.resource_name).to eq "Cart" }
    
  describe 'menu' do
    it { expect(resource).to be_include_in_menu }
  end

  describe 'actions' do
    it { expect(resource.defined_actions).to match_array [:update, :edit, :index, :show, :destroy] }

    context 'GET #index' do
      subject { get :index }
      it_should_behave_like "default action behavior", :index
    end

    context 'GET #show' do
      subject { get :show, id: create(:cart) }
      it_should_behave_like "default action behavior", :show
    end
  end
end