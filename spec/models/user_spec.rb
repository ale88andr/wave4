require 'spec_helper'

describe User do

  subject { described_class.new }

  context 'database contains' do
    it { should have_db_column(:name).of_type(:string) }
    it { should have_db_column(:email).of_type(:string) }
    it { should have_db_column(:encrypted_password).of_type(:string) }
    it { should have_db_index(:name).unique(true) }
  end

  context 'relations' do
    it { should have_one(:profile).dependent(:destroy) }
  end

  it { should accept_nested_attributes_for(:profile).allow_destroy(true) }

  context 'validates' do
    context 'rules' do
      it { should validate_presence_of :name }
      it { should validate_presence_of :email }
      it { should validate_presence_of :password }
      it { should validate_uniqueness_of :name }
      it { should validate_uniqueness_of :email }
      it { should validate_confirmation_of :password }
      it { should ensure_length_of(:name).is_at_most(50) }
      it { should ensure_length_of(:password).is_at_least(6) }
    end

    context 'with incorrect data' do
      it { should_not allow_value("B" * 100).for(:name) }
      it { should_not allow_value("example_email").for(:email) }
      it { should_not allow_value("1" * 5).for(:password) }
      it 'duplicate username' do
        user = create(:user)
        should_not allow_value(user.name).for(:name)
      end
    end

    context "with correct data" do
      it { should allow_value("Mr. Jared D'Entony").for(:name) }
      it { should allow_value("example@mail.com").for(:email) }
      it { should allow_value("Pa$$w0rd").for(:password) }
      it { expect(FactoryGirl.build(:user)).to be_valid }
    end
  end
end