require 'spec_helper'

describe Feature do
  subject { described_class.new }

  describe 'database fields' do
    it { expect(subject).to have_db_column(:name).of_type(:string) }
    it { expect(subject).to have_db_column(:label).of_type(:string) }
    it { expect(subject).to have_db_column(:description).of_type(:text) }
  end

  describe 'relation' do
    it { expect(subject).to have_and_belong_to_many(:entities) }
  end

  describe 'validates' do
    context 'rules' do
      it { should validate_presence_of(:name) }
      it { should validate_uniqueness_of(:name).case_insensitive }
      it { should ensure_length_of(:name).is_at_most(50) }
    end

    context 'should passes with correct data' do
      it { expect(build :feature).to be_valid }
    end

    context 'should fails with wrong data' do
      it 'duplicate feature name' do
        feature = create :feature
        expect(subject).not_to allow_value(feature.name).for(:name)
      end
      it { expect(subject).not_to allow_value('a' * 80).for(:name) }
    end
  end
end
