require 'spec_helper'

describe Unit do
  subject { described_class.new }

  describe 'database fields' do
    it { expect(subject).to have_db_column(:param).of_type(:string) }
  end

  describe 'relations' do
    it { should have_many(:attr) }
  end

  describe 'validations' do
    context 'rules' do
      it { should validate_presence_of(:param) }
      it { should validate_uniqueness_of(:param).case_insensitive }
      it { should ensure_length_of(:param).is_at_most(15) }
    end

    context 'with correct data values' do
      it { expect(build :unit).to be_valid }
    end

    context 'with incorrect data values' do
      it 'duplicate attribute name' do
        unit = create(:unit)
        should_not allow_value(unit.param).for(:param)
      end
      it { should_not allow_value('a' * 20).for(:param) }
    end
  end
end
