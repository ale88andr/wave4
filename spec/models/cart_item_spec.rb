require 'spec_helper'

describe CartItem do
  subject { described_class.new }

  describe 'database contains' do
    it { should have_db_index(:entity_id) }
    it { should have_db_index(:cart_id) }
    it { should have_db_column(:quantity).of_type(:integer).with_options(default: 1) }
  end

  describe 'relations' do
    it { should belong_to(:entity) }
    it { should belong_to(:cart) }
  end

  describe 'methods' do
    context 'total_item_price' do
      let(:quantity)    { 2 }
      let!(:entity)     { create :entity }
      let!(:cart_item)  { create :cart_item, entity_id: entity, quantity: quantity }

      before { cart_item.stub(:entity).and_return(entity) }

      it 'item price must equal entity price * quantity' do
        expect(cart_item.total_item_price).to eq entity.price * quantity
      end
    end
  end
end
