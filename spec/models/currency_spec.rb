require 'spec_helper'

describe Currency do

  subject { described_class.new }

  context 'database columns' do
    it { should have_db_column(:name).of_type(:string) }
    it { should have_db_column(:abbreviation).of_type(:string) }
    it { should have_db_column(:display).of_type(:boolean) }
    it { should have_db_column(:ratio).of_type(:decimal).with_options(precision: 5, scale: 2) }
    it { should have_db_column(:emblem).of_type(:string) }
    it { should have_db_column(:prime_descriptor).of_type(:boolean) }
    it { should have_db_column(:sec_descriptor).of_type(:boolean) }
    it { should have_db_column(:updated_at).of_type(:datetime) }
    it { should have_db_column(:created_at).of_type(:datetime) }
  end

  context 'relations' do
    it { should have_many(:entities) }
  end

  describe 'class methods' do
    
    it '.set_currency_abbreviation' do
      currency = create(:currency)
      expect(Currency.find(currency).abbreviation).to eq(Currency::TYPES.key(currency.name).to_s)
    end

    context 'currency description' do

      let!(:currencies) { create :currency }

      xit '.set_currency_descriptor' do

      end

      it '.set_no_description' do
        db_cur = Currency.find(currencies)
        expect(db_cur.prime_descriptor && db_cur.sec_descriptor).to be_false
      end

      context '.set_primary_description' do

        let(:cur) { create :currency, prime_descriptor: true }

        it 'setting up to true' do
          expect(Currency.find(cur).prime_descriptor).to be
        end

        it 'should be single' do
          expect(Currency.main.count).to eq 1
        end

        it 'should not have secondary description' do
          expect(Currency.find(cur).sec_descriptor).to be_false
        end
      end

      context '.set_secondary_description' do
        pending
      end
    end
  end

  describe 'validations' do
    context 'rules' do
      it { should validate_presence_of(:name) }
      it { should validate_uniqueness_of(:name), case_insensitive: false }
      it { should ensure_inclusion_of(:name).in_array(Currency::TYPES.values) }
      it { should validate_numericality_of(:ratio) }
    end

    context 'with incorrect data' do
      it { should_not allow_value('not in Currency::TYPES hash').for(:name) }

      it 'duplicate currency' do
        currency = create(:currency)
        should_not allow_value(currency.name).for(:name)
      end

      it { should_not allow_value('').for(:name) }
      it { should_not allow_value('a').for(:ratio) }
    end

    context 'with correct data' do
      it { expect(build(:currency)).to be_valid }
    end
  end
end
