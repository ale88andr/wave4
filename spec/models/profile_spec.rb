require 'spec_helper'

describe Profile do

  subject { described_class.new }

  context 'database fields' do
    it { should have_db_column(:username).of_type(:string) }
    it { should have_db_column(:email_visible).of_type(:boolean).with_options(default: false) }
    it { should have_db_column(:about).of_type(:text) }
    it { should have_db_column(:address).of_type(:string) }
    it { should have_db_column(:phone).of_type(:string) }
    it { should have_db_column(:gender).of_type(:boolean) }
    it { should have_db_column(:birthday).of_type(:date) }
    it { should have_db_column(:time_zone).of_type(:string) }
    it { should have_db_column(:skype).of_type(:string) }
    it { should have_db_column(:url).of_type(:string) }
    it { should have_db_column(:avatar).of_type(:string) }
  end

  context 'relations' do
    it { should belong_to(:user) }
  end

  it { should serialize :address }

  context 'validation rules' do
    it { should ensure_length_of(:username).is_at_most(25) }
    it { should ensure_length_of(:phone).is_at_most(25) }
    it { should ensure_length_of(:skype).is_at_most(25) }
    it { should allow_value('http://someone.com/id-2243242', 'https://www.cc.de/crap/').for(:url) }
  end

  context 'validation' do
    context 'failed with' do
      it { should_not allow_value("a" * 40).for(:username) }
      it { should_not allow_value("5" * 40).for(:phone) }
      it { should_not allow_value("skype" * 40).for(:skype) }
      it { should_not allow_value('asdfjkl').for(:url) }
    end

    context 'passed with' do
      it { expect(FactoryGirl.build :profile).to be_valid }
    end
  end
end