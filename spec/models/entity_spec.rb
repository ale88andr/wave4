require 'spec_helper'

describe Entity do

  subject { described_class.new }

  context 'database fields' do
    it { expect(subject).to have_db_column(:title).of_type(:string) }
    it { expect(subject).to have_db_column(:description).of_type(:text) }
    it { expect(subject).to have_db_column(:price).of_type(:decimal).with_options(precision: 5, scale: 2) }
    it { expect(subject).to have_db_column(:price_exchange).of_type(:boolean).with_options(null: false, default: false) }
    it { expect(subject).to have_db_column(:description).of_type(:text) }
    it { expect(subject).to have_db_column(:cover).of_type(:string) }
    it { expect(subject).to have_db_column(:published).of_type(:boolean).with_options(null: false, default: true) }
    it { expect(subject).to have_db_column(:views).of_type(:integer) }
    it { expect(subject).to have_db_column(:characteristics).of_type(:text) }
    it { expect(subject).to have_db_column(:availability).of_type(:integer) }
    it { expect(subject).to have_db_column(:guarantee).of_type(:integer) }
    it { expect(subject).to have_db_column(:price_end_date).of_type(:date) }
  end

  context 'database index' do
    it { expect(subject).to have_db_index(:title).unique(true) }
    it { expect(subject).to have_db_index(:currency_id) }
    it { expect(subject).to have_db_index(:category_id) }
    it { expect(subject).to have_db_index(:manufacturer_id) }
  end

  context "nested attributes" do
    it { should accept_nested_attributes_for(:parameters) }
  end

  describe 'model assosiation with:' do
    it { should belong_to(:category) }
    it { should belong_to(:manufacturer) }
    it { should belong_to(:currency) }

    it { should have_many(:entity_properties).through(:parameters).class_name('Attribute') }
    it { should have_many(:parameters).dependent(:destroy) }

    it { should have_and_belong_to_many(:features) }
  end

  describe 'validations' do
    context 'rules' do
      it { should validate_presence_of(:title) }
      it { should validate_uniqueness_of(:title) }
      it { should validate_numericality_of(:price) }
      it { should validate_numericality_of(:availability).only_integer }
      it { should validate_numericality_of(:guarantee).only_integer }
    end

    context 'with correct data values' do
      it { expect(build :entity).to be_valid }
    end

    context 'with incorrect data values' do
      it 'duplicate title' do
        entity = create(:entity)
        should_not allow_value(entity.title).for(:title)
      end
      it { should_not allow_value('a' * 256).for(:title) }
      it { should_not allow_value('a', -256.15).for(:price) }
      it { should_not allow_value('a', -20, 5.5).for(:availability) }
      it { should_not allow_value('20', -1).for(:guarantee) }
    end
  end

  context 'instance methods' do
    let!(:currency) { create :currency, prime_descriptor: true }
    let!(:product) { create(:entity, currency: nil) }

    it '#handle_title should titelize product name' do
      expect(Entity.find(product).title).to eq product.title.titlecase
    end

    xit '#set_product_currency must set default currency' do
      expect(Entity.find(product).currency_id).to eq currency.id
    end
  end
end
