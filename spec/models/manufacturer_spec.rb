require 'spec_helper'

describe Manufacturer do
  subject { described_class.new }

  describe 'database fields' do
    it { expect(subject).to have_db_column(:name).of_type(:string) }
    it { expect(subject).to have_db_column(:thumb).of_type(:string) }
    it { expect(subject).to have_db_column(:description).of_type(:text) }
    it { expect(subject).to have_db_column(:url).of_type(:string) }
  end

  describe 'relations' do
    it { should have_many(:entities) }
  end

  describe 'validations' do
    context 'rules' do
      it { should validate_presence_of(:name) }
      it { should validate_uniqueness_of(:name).case_insensitive }
      it { should ensure_length_of(:name).is_at_most(150) }
    end

    context 'with correct data values' do
      it { expect(build :manufacturer).to be_valid }
    end

    context 'with incorrect data values' do
      it 'duplicate manufacturer name' do
        manufacturer = create(:manufacturer)
        expect(subject).not_to allow_value(manufacturer.name).for(:name)
      end
      it { expect(subject).not_to allow_value('a' * 180).for(:name) }
      it { expect(subject).to allow_value('http://someone.com/id-2243242', 'https://www.cc.de/crap/').for(:url) }
    end
  end
end
