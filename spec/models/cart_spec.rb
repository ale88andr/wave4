require 'spec_helper'

describe Cart do

  subject { described_class.new }

  describe 'database contains' do
    it { expect(subject).to have_db_index(:user_id) }
    it { expect(subject).to have_db_index(:admin_user_id) }
    it { expect(subject).to have_db_column(:user_confirmation).of_type(:boolean) }
    it { expect(subject).to have_db_column(:admin_user_confirmation).of_type(:boolean) }
    it { expect(subject).to have_db_column(:cart_info).of_type(:string) }
  end

  describe 'relations' do
    it { expect(subject).to have_many(:cart_items).dependent(:destroy) }
  end

  describe 'serialize' do
    it { expect(subject).to serialize(:cart_info).as(Hash) }
  end

  describe 'validate' do
    let!(:cart) { attributes_for :cart_confirm }

    context 'with invalid data' do
      it "failed with empty username" do
        cart[:cart_info][:username] = ' '
        expect(build :cart, cart_info: cart[:cart_info]).not_to be_valid
      end

      it "failed with empty username" do
        cart[:cart_info][:phone] = nil
        should_not allow_value(cart[:cart_info]).for(:cart_info).on(:update)
      end

      it "failed with empty username" do
        cart[:cart_info][:adress] = nil
        should_not allow_value(cart[:cart_info]).for(:cart_info).on(:update)
      end

      it "failed with empty username" do
        cart[:cart_info][:username] = rand(0..999)
        should_not allow_value(cart[:cart_info]).for(:cart_info).on(:update)
      end

      it "failed with empty username" do
        cart[:cart_info][:phone] = 'some string'
        should_not allow_value(cart[:cart_info]).for(:cart_info).on(:update)
      end
    end

    context 'with valid data' do
      it { expect(build :cart, cart_info: cart[:cart_info]).to be_valid }
    end
  end

  describe 'instance methods' do
    context '.add_product' do
      let!(:entity)     { create :entity }
      let!(:cart_item)  { create :cart_item, entity: entity }
      let!(:cart)       { create :cart, cart_items: [cart_item] }

      before { cart.stub_chain(:cart_items, :find_by).and_return(cart_item) }

      xit "increment cart_item quantity if double added product" do
        cart.add_product(entity.id)
        expect(CartItem.find_by(entity_id: entity.id).quantity).to eq cart_item.quantity.next
      end

      it "assigns the requested entities to current_item" do
        expect(cart.add_product(entity.id)).to eq cart_item
      end
    end

    context 'total_cart_price' do
      let!(:cart_items) { create_list :cart_item, 2 }
      let!(:cart)       { create :cart, cart_items: cart_items }

      before { cart.stub(:cart_items).and_return(cart_items) }

      xit "must return summ of a all cart items" do
        expect(cart.total_cart_price).to eq cart_items.sum { |item| item.total_item_price }
      end
    end
  end
end
