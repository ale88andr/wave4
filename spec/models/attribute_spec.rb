require 'spec_helper'

describe Attribute do
  subject { described_class.new }

  describe 'database fields' do
    it { expect(subject).to have_db_column(:name).of_type(:string) }
    it { expect(subject).to have_db_column(:unit_id).of_type(:integer) }
  end

  describe 'relations' do
    it { should have_and_belong_to_many(:categories) }

    it { should belong_to(:unit) }

    it { should have_many(:parameters) }
    it { should have_many(:entities).through(:parameters) }
  end

  describe 'validations' do
    context 'rules' do
      it { should validate_presence_of(:name) }
      it { should validate_uniqueness_of(:name).case_insensitive }
      it { should ensure_length_of(:name).is_at_most(25) }
    end

    context 'with correct data values' do
      it { expect(build :attribute).to be_valid }
    end

    context 'with incorrect data values' do
      it 'duplicate attribute name' do
        attribute = create(:attribute)
        should_not allow_value(attribute.name).for(:name)
      end
      it { should_not allow_value('a' * 26).for(:name) }
      it { should_not allow_value('a!@shKiDD', '$um4', '!a~w0rd').for(:name) }
    end
  end

  context 'instance methods' do
    let!(:attribute) { create(:attribute) }

    it '#handle_name should titelize attribute name' do
      expect(Attribute.find(attribute).name).to eq attribute.name.titlecase
    end
  end
end
