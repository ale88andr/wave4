require 'spec_helper'

describe Category do

  subject { described_class.new }

  context 'database contains' do
    it { expect(subject).to have_db_column(:title).of_type(:string) }
    it { expect(subject).to have_db_column(:description).of_type(:text) }
    it { expect(subject).to have_db_column(:active).of_type(:boolean) }
    it { expect(subject).to have_db_column(:order).of_type(:integer) }
    it { expect(subject).to have_db_column(:parent_id).of_type(:integer) }
    it { expect(subject).to have_db_column(:created_at).of_type(:datetime) }
  end

  context 'relations' do
    it { expect(subject).to belong_to(:parent_category).class_name('Category').dependent(:destroy).with_foreign_key('parent_id') }
    it { expect(subject).to have_many(:subcategories).class_name('Category').with_foreign_key('parent_id') }
    it { expect(subject).to have_many(:entities).dependent(:destroy) }
    it { expect(subject).to have_and_belong_to_many(:properties).class_name('Attribute') }
  end

  describe 'class methods' do
    let!(:category) { create(:category, title: 'oQuttEr') }

    it '.title_handler' do
      expect(Category.find(category.id).title).to eq('Oqutter')
    end

    it '.set_start_order' do
      expect(Category.find(category.id).order).to eq 1
    end
  end

  describe 'validations' do
    context 'rules' do
      it { expect(subject).to validate_presence_of(:title) }
      it { expect(subject).to validate_uniqueness_of(:title), case_insensitive: false }
      it { expect(subject).to validate_numericality_of(:order) }
      it { expect(subject).to ensure_length_of(:title).is_at_most(30) }
    end

    context 'with incorrect data' do
      it { expect(subject).not_to allow_value('a' * 100).for(:title) }

      it 'duplicate title' do
        category = create(:category)
        expect(subject).not_to allow_value(category.title).for(:title)
      end

      it { expect(subject).not_to allow_value('@astra', 'Role$', 'F0r$$ed').for(:title) }
      it { expect(subject).not_to allow_value('').for(:title) }
      it { expect(subject).not_to allow_value('a').for(:order) }
    end

    context 'with correct data' do
      it { expect(build(:category)).to be_valid }
    end
  end
end
