def login_as_user(user=nil)
  @user = user
  @user ||= FactoryGirl.create :user
  sign_in @user

  @user
end
