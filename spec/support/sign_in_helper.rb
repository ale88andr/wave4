module SignInHelper
  def sign_in_active_admin(user=nil)
  	@user = user
  	@user ||= create :admin_user
    visit new_admin_user_session_path
    fill_in 'admin_user[login]', 	with: @user.email
    fill_in 'admin_user[password]', with: @user.password
    click_button 'Login'
  end

  alias_method :login_as_admin, :sign_in_active_admin
end