require 'spec_helper'

describe "Functional of Admin User" do

  describe "Admin user" do

    Given { login_as_admin }

    context 'should not be available' do
      When { visit admin_admin_users_path }
      Then { expect(page).not_to have_link href: new_admin_admin_user_path }
    end
  end

  describe "Super admin user" do

    Given { login_as_admin(create :admin_user, super_admin: true) }

    describe "Create new unit" do

      Given!    (:admin_user) { attributes_for :admin_user }
      Given     { visit new_admin_admin_user_path }
      Invariant { expect(current_path).to eq '/admin/admin_users/new' }
      Invariant { expect(page).to have_selector 'form#new_admin_user' }

      context "filling admin_user form" do
        context "with valid parameters" do
          When do
            within '#new_admin_user' do
              fill_in 'admin_user[email]',                 with: admin_user[:email]
              fill_in 'admin_user[password]',              with: admin_user[:password]
              fill_in 'admin_user[password_confirmation]', with: admin_user[:password_confirmation]
            end
          end
          Then { expect(page).to have_content 'sdgffdsgdfg' }
          # Then { expect{ click_button 'Create Admin User' }.to change(AdminUser, :count).by(1) }
          And  { expect(current_path).to eq admin_admin_user_path(AdminUser.last) }
        end

        context "with wrong parameters" do
          When  { fill_in 'admin_user[email]', with: admin_user[:email] }
          Then  { expect{ click_button 'Create AdminUser' }.not_to change(Unit, :count) }
          And   { expect(page).to have_selector 'form#new_admin_user' }
        end
      end
    end

  end

  # describe "Show single unit" do

  #   Given (:unit) { create :unit }
    
  #   context 'should be excluded' do
  #     Then { expect{ visit admin_unit_path(unit) }.to raise_exception ActionController::RoutingError }
  #   end
  # end

  # describe "Delete unit" do

  #   Given!    (:unit) { create :unit }
  #   Given     { visit admin_units_path }
  #   Invariant { expect(page).to have_content unit.param }
  #   Invariant { expect(page).to have_link "Delete", href: admin_unit_path(unit) }

  #   context 'from :index' do
  #     Then  { expect{ click_link 'Delete' }.to change(Unit, :count).by(-1) }
  #     And   { expect{ Unit.find(unit.id) }.to raise_error ActiveRecord::RecordNotFound }
  #   end
  # end

  # describe "Display all units" do

  #   context 'with few units' do
  #     Given!  (:units) { create_list :unit, 5 }
  #     Given   { visit admin_units_path }
  #     Then do
  #       units.each do |unit|
  #         expect(page).to have_content unit.param
  #       end
  #     end
  #     And { expect(page).to have_link 'New Unit', href: new_admin_unit_path }
  #   end

  #   context 'with no units' do
  #     When  { visit admin_units_path }
  #     Then  { expect(page).to have_content 'There are no Units yet' }
  #     And   { expect(page).to have_link 'Create one', href: new_admin_unit_path }
  #   end
  # end

  # describe "Edit unit" do

  #   Given!    (:unit) { create :unit }
  #   Given     { visit edit_admin_unit_path(unit) }

  #   context 'filling edit form with valid parameters' do
  #     Given (:edit_unit) { attributes_for :unit }
  #     When do
  #       fill_in 'unit[param]', with: edit_unit[:param]
  #       click_button "Update Unit"
  #     end
  #     Then  { expect(current_path).to eq admin_units_path }
  #     And   { expect(page).to have_content edit_unit[:param] }
  #   end

  #   context 'filling edit form with wrong parameters' do
  #     When do
  #       fill_in 'unit[param]', with: nil
  #       click_button "Update Unit"
  #     end
  #     Then  { expect(current_path).to eq "/admin/units/#{unit.id}" }
  #   end
  # end
end