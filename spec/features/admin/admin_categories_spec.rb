require 'spec_helper'

describe "Categories module" do

  Given { login_as_admin }

  describe "Create category" do

    Given!  (:attributes) { create_list :attribute, 3 }
    Given   (:category_attr) { attributes_for :category }
    Given   { visit new_admin_category_path }
    Invariant { expect(current_path).to eq '/admin/categories/new' }
    Invariant { expect(page).to have_selector 'form#new_category' }

    context "with valid parameters" do
      When do
        fill_in 'category[title]',        with: category_attr[:title]
        fill_in 'category[description]',  with: category_attr[:description]
        select  'Category Active Enable', from: 'category[active]'
        select  'Category Is Parent',     from: 'category[parent_id]'
      end
      Then { expect{ click_button 'Create Category' }.to change(Category, :count).by(1) }
      And  { expect(current_path).to eq admin_category_path(Category.last) }
    end

    context "with invalid parameters" do
      When { fill_in 'category[title]', with: nil }
      Then { expect{ click_button 'Create Category' }.not_to change(Category, :count) }
      And  { expect(page).to have_content 'New Category' }
    end
  end

  describe "Show category" do

    context 'with valid id' do

      Given (:category) { create :category_with_attributes }
      When  { visit admin_category_path(category) }

      context 'should display category name' do
        Then  { expect(page).to have_content category.title }
      end

      context 'should display category actions' do
        Then  { expect(page).to have_link 'Edit Category', href: edit_admin_category_path(category) }
        Then  { expect(page).to have_link 'Delete Category', href: admin_category_path(category) }
      end

      context 'should display attribute active admin comments' do
        it_behaves_like "active admin comments"
      end

      context 'should display category attributes' do
        Then do
          category.properties.each do |prop|
            expect(page).to have_content prop.name
          end
        end
      end
    end

    context 'with invalid id' do
      Then { expect{ visit "/admin/attributes/id_0911" }.to raise_exception ActiveRecord::RecordNotFound }
    end
  end

  describe "Delete category" do

    Given! (:category) { create :category }

    shared_examples_for 'expect that category delete' do |link_name|
      Then  { expect{ click_link link_name }.to change(Category, :count).by(-1) }
      And   { expect{ Category.find(category.id) }.to raise_error ActiveRecord::RecordNotFound }
    end

    context 'from :index' do
      When  { visit admin_categories_path }
      it_behaves_like "expect that category delete", 'Delete'
    end

    context 'from :show' do
      When  { visit admin_category_path(category) }
      it_behaves_like "expect that category delete", 'Delete Category'
    end
  end

  describe 'display all categories' do

    context 'exists categories' do
      Given! (:categories) { create_list :category, 5 }
      When   { visit admin_categories_path }

      Then do
        categories.each do |category|
          expect(page).to have_link category.title, href: admin_category_path(category)
        end
      end
      Then { expect(page).to have_link 'New Category', href: new_admin_category_path }

      context 'scopes' do
        Then { expect(page).to have_link 'All' }
        Then { expect(page).to have_link 'Parental' }
        Then { expect(page).to have_link 'Active' }
      end
    end

    context 'not exists categories' do
      When { visit admin_categories_path }
      Then { expect(page).to have_content 'There are no Categories yet' }
      Then { expect(page).to have_link 'Create one', href: new_admin_category_path }
    end
  end

  describe 'Sidebar' do

    Given! (:parent) { create :category }
    Given! (:subcategory) { create :category, parent_id: parent.id }

    context 'on parent category' do
      When { visit admin_category_path(parent) }
      Then { expect(page).to have_no_selector 'div#_sidebar_section' }
    end

    context 'on subcategory' do
      shared_examples_for 'display sidebar' do
        Then do
          within 'div#_sidebar_section' do
            expect(page).to have_content subcategory.title
          end
        end
      end

      context 'should display from :show' do
        When { visit admin_category_path(subcategory) }
        it_behaves_like "display sidebar"
      end
    end
  end
end