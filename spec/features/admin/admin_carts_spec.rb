require 'spec_helper'

describe "Carts" do

  Given { login_as_admin }

  describe '#index: ' do
    context 'when carts existing' do
      Given!(:carts) { create_list :cart_confirm, 3, :confirmed }
      Given(:cart) { carts.first }
      When { visit admin_carts_path }

      context 'show list of carts' do
        Then { expect(page).to have_selector 'table#index_table_carts' }
        Then { expect(page.all('table#index_table_carts tbody tr').count).to eq carts.length }
        Then { expect(page).to have_text "##{cart.id}" }
        Then { expect(page).to have_text "#{cart.cart_info[:username]}, #{cart.cart_info[:phone]}" }
      end

      context 'show carts controls' do
        Then { expect(page).to have_link "##{cart.id}", href: admin_cart_path(cart) }
        Then { expect(page).to have_link "Delete", href: admin_cart_path(cart) }
        Then { expect(page).to have_link "View", href: admin_cart_path(cart) }
      end
    end

    context 'when carts doesnt exists' do
      When { visit admin_carts_path }
      Then { expect(page).to have_text 'There are no Carts yet.' }
    end
  end

  describe '#show: ' do
    Given!(:cart) { create :full_cart }
    When { visit admin_cart_path(cart) }

    context 'should display cart items' do
      Given(:cart_item) { cart.cart_items.first }
      Given(:product) { cart_item.entity }
      Then { expect(page.all('table tbody tr').count).to eq cart.cart_items.count }
      Then { expect(page).to have_text cart_item.quantity }
      Then { expect(page).to have_link product.title, href: category_entity_path(product.category.id, product.id) }
      Then { expect(page).to have_text product.price }
    end

    context 'should display customer info' do
      Then { expect(page).to have_text  cart.cart_info[:username],
                                        cart.cart_info[:phone],
                                        cart.cart_info[:city]
                                        cart.cart_info[:street] 
                                      }
    end

    context 'render with confirm control' do
      Then { expect(page).to have_link 'Подтвердить', href: confirm_admin_cart_path(cart) }
    end
  end

  describe '#destroy' do
    Given!(:cart) { create :full_cart }
    Given { visit admin_cart_path(cart) }
    subject(:delete) { click_link 'Delete', href: admin_cart_path(cart) }

    context 'should delete cart record' do
      Then { expect{
              delete
            }.to change(Cart, :count).by(-1) }
    end

    context 'should delete cart item record' do
      Then { expect{
              delete
            }.to change(CartItem, :count).by(-1) }
    end

    context 'should be redirect to #index' do
      When { delete }
      Then { expect(current_path).to eq admin_carts_path }
      Then { expect(page).not_to have_link "##{cart.id}" }
    end
  end

  describe '#confirm' do
    context 'when user confirm is true' do
      Given!(:cart) { create :full_cart, admin_user_confirmation: false }
      Given { visit admin_cart_path(cart) }
      When { click_link 'Подтвердить', href: confirm_admin_cart_path(cart) }
      Then { expect(Cart.find(cart).admin_user_confirmation).to eq true }
      Then { expect(current_path).to eq admin_carts_path }
    end

    context 'when user confirm is false' do
      Given!(:cart) { create :cart, :with_customer_info }
      Given { visit admin_cart_path(cart) }
      Then { expect(page).not_to have_link 'Подтвердить', href: confirm_admin_cart_path(cart) }
    end
  end
end