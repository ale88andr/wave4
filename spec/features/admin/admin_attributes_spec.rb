require 'spec_helper'

describe "Functional of Attributes" do

  Given { login_as_admin }
  Given!(:unit) { create :unit }

  describe "Create new attribute" do

    Given     { visit new_admin_attribute_path }
    Invariant { expect(current_path).to eq '/admin/attributes/new' }
    Invariant { expect(page).to have_selector 'form#new_attribute' }

    context "filling attribute form" do
      context "with valid parameters" do
        Given  (:attribute) { attributes_for :attribute }
        When do
          within '#new_attribute' do
            fill_in 'attribute[name]',  with: attribute[:name]
            select  unit.param,         from: 'attribute[unit_id]'
          end
        end
        Then { expect{ click_button 'Create Attribute' }.to change(Attribute, :count).by(1) }
        And  { expect(current_path).to eq admin_attribute_path(Attribute.last) }
        And  { expect(page).to have_content attribute[:name] }
      end

      context "with wrong parameters" do
        Given (:attribute) { attributes_for :attribute, name: nil }
        When  { fill_in 'attribute[name]', with: attribute[:name] }
        Then  { expect{ click_button 'Create Attribute' }.not_to change(Attribute, :count) }
        And   { expect(page).to have_selector 'form#new_attribute' }
      end
    end
  end

  describe "Show attribute" do

    context 'with valid id' do

      Given (:attribute) { create :attribute, unit_id: unit.id }
      When  { visit admin_attribute_path(attribute) }

      context 'should display attribute name and actions' do
        Then  { expect(page).to have_content attribute.name }
      end

      context 'should display attribute actions' do
        Then  { expect(page).to have_link 'Edit Attribute', href: edit_admin_attribute_path(attribute) }
        And   { expect(page).to have_link 'Delete Attribute', href: admin_attribute_path(attribute) }
      end

      context 'should display attribute active admin comments' do
        it_behaves_like "active admin comments"
      end

      context 'should display attribute unit' do
        Then  { expect(page).to have_content unit.param.to_s.titleize }
      end
    end

    context 'with invalid id' do
      Then { expect{ visit "/admin/attributes/id_0911" }.to raise_exception ActiveRecord::RecordNotFound }
    end
  end

  describe "Delete attribute" do

    Given! (:attribute) { create :attribute }

    shared_examples_for 'expect that attribute delete' do |link_name|
      Then  { expect{ click_link link_name }.to change(Attribute, :count).by(-1) }
      And   { expect{ Attribute.find(attribute.id) }.to raise_error ActiveRecord::RecordNotFound }
    end

    context 'from :index' do
      When  { visit admin_attributes_path }
      it_behaves_like "expect that attribute delete", 'Delete'
    end

    context 'from :show' do
      When  { visit admin_attribute_path(attribute) }
      it_behaves_like "expect that attribute delete", 'Delete Attribute'
    end
  end

  describe 'display all attributes' do

    context 'exists attributes' do
      Given { @attributes = create_list :attribute, 5 }
      When  { visit admin_attributes_path }
      Then do
        @attributes.each do |attribute|
          expect(page).to have_content attribute.name
        end
      end
      And   { expect(page).to have_link 'New Attribute', href: new_admin_attribute_path }
    end

    context 'not exists attributes' do
      When  { visit admin_attributes_path }
      Then  { expect(page).to have_content 'There are no Attributes yet' }
      And   { expect(page).to have_link 'Create one', href: new_admin_attribute_path }
    end
  end

  describe "Edit attribute" do

    Given!    (:attribute) { create :attribute }
    Given     { visit edit_admin_attribute_path(attribute) }

    context 'filling edit form with valid parameters' do
      Given (:edit_attr) { attributes_for :attribute }
      When do
        fill_in 'attribute[name]', with: edit_attr[:name]
        click_button "Update Attribute"
      end
      Then  { expect(current_path).to eq admin_attribute_path(attribute) }
      And   { expect(page).to have_content edit_attr[:name] }
    end

    context 'filling edit form with wrong parameters' do
      When do
        fill_in 'attribute[name]', with: nil
        click_button "Update Attribute"
      end
      Then  { expect(current_path).to eq "/admin/attributes/#{attribute.id}" }
    end
  end

  describe 'Sidebar' do

    Given! (:attribute) { create :attribute }
    Given! (:categories) { create_list :category, 3 }

    shared_examples_for 'display sidebar' do
      Then do
        within 'div#_sidebar_section' do
          categories.each do |category|
            expect(page).to have_content category.title
          end
        end
      end
    end

    context 'should display from :index' do
      When { visit admin_attributes_path }
      it_behaves_like "display sidebar"
    end

    context 'should display from :show' do
      When { visit admin_attribute_path(attribute) }
      it_behaves_like "display sidebar"
    end
  end
end
