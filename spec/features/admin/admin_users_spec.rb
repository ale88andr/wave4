require 'spec_helper'

describe "Functional of Users" do

  Given { login_as_admin }

  describe "Create user" do

    Given   (:user) { attributes_for :user_with_profile }
    Given   { visit new_admin_user_path }
    Invariant { expect(current_path).to eq '/admin/users/new' }
    Invariant { expect(page).to have_selector 'form#new_user' }

    context "with valid parameters without profile" do
      When do
        fill_in 'user[name]',                   with: user[:name]
        fill_in 'user[email]',                  with: user[:email]
        fill_in 'user[password]',               with: user[:password]
        fill_in 'user[password_confirmation]',  with: user[:password_confirmation]
      end
      Then { expect{ click_button 'Create User' }.to change(User, :count).by(1) }
      And  { expect(current_path).to eq admin_user_path(User.last) }
    end

    context "with valid parameters with profile" do
      When do
        fill_in 'user[name]',                   with: user[:name]
        fill_in 'user[email]',                  with: user[:email]
        fill_in 'user[password]',               with: user[:password]
        fill_in 'user[password_confirmation]',  with: user[:password_confirmation]

        #profile
        fill_in 'user[profile_attributes][username]', with: user[:username]
        fill_in 'user[profile_attributes][phone]',    with: user[:phone]
        fill_in 'user[profile_attributes][skype]',    with: user[:skype]
        fill_in 'user[profile_attributes][url]',      with: user[:url]
        check   'user[profile_attributes][email_visible]'
      end
      Then { expect{ click_button 'Create User' }.to change(User, :count).by(1) }
      And  { expect(current_path).to eq admin_user_path(User.last) }
    end

    context "with invalid parameters" do
      When { fill_in 'user[name]', with: nil }
      Then { expect{ click_button 'Create User' }.not_to change(User, :count) }
      And  { expect(page).to have_content 'New User' }
    end
  end

  describe 'display all users' do

    context 'exists users' do
      Given! (:users) { create_list :user, 5 }
      When   { visit admin_users_path }

      Then do
        users.each do |user|
          expect(page).to have_content user.name
          expect(page).to have_content user.email
        end
      end
      Then { expect(page).to have_link 'New User', href: new_admin_user_path }
    end

    context 'not exists users' do
      When { visit admin_users_path }
      Then { expect(page).to have_content 'There are no Users yet' }
      Then { expect(page).to have_link 'Create one', href: new_admin_user_path }
    end
  end

  describe "Show user" do

    context 'existing user' do

      Given! (:user) { create :user_with_profile }
      When   { visit admin_user_path(user) }

      context 'should display user model attributes' do
        Then  { expect(page).to have_selector 'h2', text: user.name }
        Then  { expect(page).to have_selector 'b', text: user.email }
      end

      context 'should display user profile attributes' do
        Then  { expect(page).to have_content user.profile.username }
        Then  { expect(page).to have_content user.profile.phone }
        Then  { expect(page).to have_content user.profile.skype }
        Then  { expect(page).to have_content user.profile.url }
        Then  { expect(page).to have_content user.profile.about }
        Then  { expect(page).to have_content user.profile.time_zone }
        Then  { expect(page).to have_content user.profile.avatar }
      end

      context 'should display user actions' do
        Then  { expect(page).to have_link 'Edit User', href: edit_admin_user_path(user) }
        Then  { expect(page).to have_link 'Delete User', href: admin_user_path(user) }
      end

      context 'should display attribute active admin comments' do
        it_behaves_like "active admin comments"
      end
    end

    context 'with invalid id' do
      Then { expect{ visit "/admin/users/id_0911" }.to raise_exception ActiveRecord::RecordNotFound }
    end
  end

  describe "Delete user" do

    Given! (:user) { create :user_with_profile }

    shared_examples_for 'expect that user and his profile delete' do |link_name|
      Then  { expect{ click_link link_name }.to change(User, :count).by(-1) }
      And   { expect{ User.find(user.id) }.to raise_error ActiveRecord::RecordNotFound }
      And   { expect{ Profile.find(user.profile) }.to raise_error ActiveRecord::RecordNotFound }
    end

    context 'from :index' do
      When  { visit admin_users_path }
      it_behaves_like "expect that user and his profile delete", 'Delete'
    end

    context 'from :show' do
      When  { visit admin_user_path(user) }
      it_behaves_like "expect that user and his profile delete", 'Delete User'
    end
  end
end