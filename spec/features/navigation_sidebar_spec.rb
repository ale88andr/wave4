require 'spec_helper'

describe 'display navigation' do
  context 'with active categories' do
    Given do
      @category = create :category
      # subcategory = create :category, parent_id: category.id
    end
    When { visit root_path }
    Then do
      within('.nav-pills', '.nav-stacked') do
        expect(page).to have_link 'Категории', href: store_categories_path
        expect(page).to have_content @category.title
      end
    end
  end

  context 'with disabled categories' do
    Given do
      @category = create :category, active: false
    end
    When { visit root_path }
    Then do
      within('.nav-pills', '.nav-stacked') do
        expect(page).not_to have_content @category.title
      end
    end
  end
end