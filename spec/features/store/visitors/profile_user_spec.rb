require 'spec_helper'

describe 'user profile' do
  context 'show detail view' do
    Given(:user) { create :user }
    Given(:profile) { create :profile, user: user }
    When  { visit store_user_path(user) }
    Then do
      within '.page-header' do
        expect(page).to have_content user.name
      end
    end
    Then do
      expect(page).to have_content user.email
      expect(page).to have_content profile.username
      expect(page).to have_content profile.birthday
      expect(page).to have_content profile.url
      expect(page).to have_content profile.phone
    end
  end
end