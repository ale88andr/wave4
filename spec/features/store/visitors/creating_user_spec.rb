require 'spec_helper'

describe 'follow sign up steps' do
  context 'guest visit register form' do
    Given { visit root_path }
    Given { expect(page).to have_link 'Присоединиться', href: new_user_registration_path }
    When  { click_link('Присоединиться') }
    Then  { expect(current_path).to eq '/signup' }
    Then do
      within('#new_user') do
        expect(page).to have_field 'user[name]',                  type: 'text' 
        expect(page).to have_field 'user[email]',                 type: 'email'
        expect(page).to have_field 'user[password]',              type: 'password'
        expect(page).to have_field 'user[password_confirmation]', type: 'password'
      end
    end
  end

  context 'register new user' do

    Given(:user) { attributes_for(:user) }

    context 'with valid data and redirect after sign up to new profile' do
      Given { visit new_user_registration_path }
      Given(:profile) { attributes_for(:profile) }
      When do
        within '#new_user' do
          fill_in 'user_name',                  with: user[:name]
          fill_in 'user_email',                 with: user[:email]
          fill_in 'user_password',              with: user[:password]
          fill_in 'user_password_confirmation', with: user[:password_confirmation]
        end
      end
      Then { expect{ click_on "sign_up" }.to change(User, :count).by(1) }

      context 'profile' do
        When { click_on "sign_up" }
        Then do
          within 'form#new_profile' do
            fill_in 'profile_username',       with: profile[:username]
            check   'profile_email_visible'
            fill_in 'profile_phone',          with: profile[:phone]
            fill_in 'profile_skype',          with: profile[:skype]
            fill_in 'profile_url',            with: profile[:url]
            select  'Севастополь',            from: 'profile_address_city'
            fill_in 'profile_address_street', with: profile[:address][:street]
            # attach_file('profile_avatar', profile[:avatar])
          end
        end
        Then do
          expect{ click_on 'create_profile' }.to change(Profile, :count).by(1)
          expect(current_path).to eq root_path
        end
      end
    end

    context 'with invalid data' do
      Given { visit new_user_registration_path }
      When do
        within '#new_user' do
          fill_in 'user_name',                  with: ''
          fill_in 'user_email',                 with: user[:email]
          fill_in 'user_password',              with: user[:password]
          fill_in 'user_password_confirmation', with: user[:password_confirmation]
        end
      end
      Then { expect{ click_on "sign_up" }.not_to change(User, :count) }
    end
  end
end
