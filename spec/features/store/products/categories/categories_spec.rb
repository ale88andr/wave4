require 'spec_helper'

describe 'displaing categories' do
  context 'list of main categories'do
    Given { @categories = create_list :category, 5 }
    Given { @inactive_categories = create_list :category, 2, :inactive }
    When  { visit store_categories_path }
    Then do
      expect(page).to have_selector 'h3', text: 'Перечень доступных категорий :'
      within '.thumbnails' do
        @categories.each do |category|
          expect(page).to have_link category.title, href: store_category_path(category)
        end
      end
    end
    Then do
      @inactive_categories.each do |disabled|
        expect(page).to have_no_content disabled
      end
    end
  end

  context 'show single category unit' do
    Given (:category) { create :category }
    When  { visit store_category_path(category) }
    Then { expect(page).to have_content 'Категория:' }
    Then { expect(page).to have_selector 'h2', text: category.title }
    Then { expect(page).to have_selector 'div', text: category.description }
  end
end