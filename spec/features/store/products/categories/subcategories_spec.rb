require 'spec_helper'

describe 'displaing subcategories' do

  Given!(:category) { create :category }
  Given!(:subcategory) { create :category, parent_id: category.id }

  context ':index'do
    When { visit store_categories_path }
    Then { expect(page).to have_selector 'h3', text: 'Перечень доступных категорий :' }
    Then { expect(page).to have_link category.title, href: store_category_path(category) }
    Then { expect(page).to have_selector 'span', text: category.subcategories.size }
  end

  context ':show subcategory unit' do
    When  { visit store_category_path(subcategory) }
    Then  { expect(page).to have_link category.title, href: store_category_path(category) }
    Then  { expect(page).to have_selector 'h2', text: subcategory.title }
    Then  { expect(page).to have_selector 'div', text: subcategory.description }
  end
end