require 'spec_helper'

describe 'Entities' do

  Given(:main_cat)  { create :category }
  Given(:category)  { create :category_with_attributes, parent_id: main_cat.id }
  Given(:currency)  { create :currency }
  Given(:entity)    { create :entity, category: category, currency: currency }

  describe 'GET #show' do

    When  { visit store_category_entity_path(category.id, entity.id) }

    context 'category info: ' do
      Then  { expect(page).to have_link category.title, href: store_category_path(category) }
      Then  { expect(page).to have_link main_cat.title, href: store_category_path(main_cat) }
    end

    context 'heading: ' do
      Then do
        expect(page).to have_selector 'span', text: entity.id
        expect(page).to have_selector 'h2',   text: entity.title
        expect(page).to have_selector 'h1',   text: entity.price
        expect(page).to have_selector 'span', text: entity.views.next
        expect(page).to have_selector 'p',    text: entity.characteristics
        expect(page).to have_selector 'p',    text: entity.description
      end
    end

    context 'heading attributes: ' do

    end

    context 'heading features' do

    end

    context 'heading buy button' do
      Then  { expect(page).to have_button 'Купить' }
    end

    context 'sidebar' do
      Then  { expect(page).to have_link main_cat.title, href: store_category_path(main_cat.id) }
      Then  { expect(page).to have_link category.title, href: store_category_path(category.id) }
    end
  end
end