require 'spec_helper'

describe 'Entities' do

  Given(:category)  { create :category_with_attributes }
  Given(:currency)  { create :currency }
  Given(:count)     { 3 }
  Given(:entities)  { create_list :entity, count, category: category, currency: currency }

  describe 'GET #index' do
    When  { visit store_category_path(entities.first.category) }
    context 'category info: ' do
      Then  { expect(page).to have_link category.title }
      And   { expect(page).to have_selector 'div', text: category.description }
    end
    context 'number of headings: ' do
      Then  { expect(page).to have_selector 'p', text: 'Товарные позиции:' }
      Then  { expect(page).to have_selector 'span.badge.badge-success', text: count }
    end
    context 'description of headings: ' do
      Then do
        entities.each do |e|
          expect(page).to have_selector 'h3', text: e.title
          expect(page).to have_selector 'h2', text: e.price
          expect(page).to have_selector 'span', text: e.currency.abbreviation
          expect(page).to have_selector 'p', text: e.characteristics
        end
      end
    end
    context 'controls: ' do
      Then { expect(page).to have_link 'Подробнее...', href: store_category_entity_path(category, entities.first.id) }
      Then { expect(page).to have_button '+ Добавить в корзину' }
    end
  end
end