require 'spec_helper'

describe 'Carts' do
  Given(:main_cat)  { create :category, parent_id: 0 }
  Given(:category)  { create :category_with_attributes, parent_id: main_cat.id }
  Given(:currency)  { create :currency }
  Given(:entity)    { create :entity, category: category, currency: currency }
  Given { visit store_category_entity_path(entity.category.id, entity.id) }
  subject(:click) { click_button 'Купить' }

  context 'add product to preview order' do
    Then { expect{click}.to change(Cart, :count).by(1) }
    Then { expect{click}.to change(CartItem, :count).by(1) }

    context 'template' do
      When { click }
      Then { expect(page).to have_link entity.title, href: store_category_entity_path(category.id, entity.id) }
      Then { expect(page).to have_selector 'b', text: entity.price }

      context 'controls' do
        Given(:item) { CartItem.find_by_entity_id(entity.id) }
        Then { expect(page).to have_link 'Удалить', href: store_cart_item_path(item) }
        Then { expect(page).to have_button 'Очистить корзину' }
        Then { expect(page).to have_link 'Оформить заказ', href: confirmation_store_cart_path(item.cart) }
      end
    end
  end

  context 'functionality: ' do
    Given { click }
    Given!(:ci) { CartItem.find_by_entity_id(entity.id) }

    context 'twice to add the same product' do
      Given { visit store_category_entity_path(entity.category.id, entity.id) }

      context 'should not add new cart items record' do
        Then { expect{ click }.not_to change(CartItem,:count) }
      end

      context 'should increment cart items quantity' do
        When { click_button 'Купить' }
        Then { expect(CartItem.find_by_entity_id(entity.id).quantity).to eq ci.quantity.next }
        Then { expect(page).to have_content(entity.price * 2) }
      end
    end

    context 'delete product' do
      subject(:delete) { click_link 'Удалить', href: store_cart_item_path(ci.id) }
      Then { expect{ delete }.to change(CartItem,:count).by(-1) }

      context 'render empty template' do
        When { delete }
        Then { expect(page).not_to have_link entity.title }
      end
    end

    context 'delete all' do
      When { click_button 'Очистить корзину' }
      Then { expect(current_path).to eq root_path }
      Then { expect{ Cart.find(ci.cart_id) }.to raise_exception ActiveRecord::RecordNotFound }
    end

    context 'confirmation order' do
      Given!(:cart) { create :cart, :with_customer_info }
      Given { visit confirmation_store_cart_path(cart) }

      context 'should render form' do
        Then { expect(page).to have_selector "form#edit_cart_#{cart.id}" }
        Then { expect(page).to have_selector "form[method='post'][action='#{create_confirmation_store_cart_path(cart)}']" }

        context 'with fields: ' do
          Then { expect(page).to have_field "cart[cart_info][username]", type: 'text' }
          Then { expect(page).to have_field "cart[cart_info][phone]", type: 'tel' }
          Then { expect(page).to have_field "cart[cart_info][note]" }
          Then { expect(page).to have_select "cart[cart_info][city]" }
          Then { expect(page).to have_field "cart[cart_info][street]", type: 'text' }
        end
      end

      context 'fill in form' do
        subject(:submit) { click_button 'Подтвердить' }
        Given(:data) { attributes_for :cart_confirm }

        context 'with valid parameters' do
          Given do
            within "form#edit_cart_#{cart.id}" do
              fill_in 'cart[cart_info][username]',  with: data[:cart_info][:username]
              fill_in 'cart[cart_info][phone]',     with: data[:cart_info][:phone]
              fill_in 'cart[cart_info][note]',      with: data[:cart_info][:note]
              select  'Севастополь',                from: 'cart[cart_info][city]'
              fill_in 'cart[cart_info][street]',    with: data[:cart_info][:street]
            end
          end
          When { submit }
          # Then { print page.html }
          Then { expect(current_path).to eq root_path }
        end

        context 'without parameters' do
          When { submit }
          Then { expect(page).to have_selector 'div.alert.alert-error' }
          Then { expect(page).to have_selector "form#edit_cart_#{cart.id}" }
        end
      end
    end
  end
end