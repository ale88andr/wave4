require 'spec_helper'
require 'carrierwave/test/matchers'

describe EntityUploader do

  include CarrierWave::Test::Matchers

  let!(:entity) { create :entity }

  before do
    image_path = "#{::Rails.root}/public/defaults/wave.png"
    AvatarUploader.enable_processing = true
    @uploader = EntityUploader.new(entity, :cover)
    @uploader.store!(File.open(image_path))
  end

  after do
    EntityUploader.enable_processing = false
    @uploader.remove!
  end

  it "should make the image readable only to the owner and not executable" do
    expect(@uploader).to have_permissions(0777)
  end

  it "thumbnail scale down image to fit within 280 by 280 pixels" do
    expect(@uploader.preview).to be_no_larger_than(160, 120)
  end

  it "thumbnail scale down image to fit within 75 by 75 pixels" do
    expect(@uploader.thumbnail).to be_no_larger_than(75, 75)
  end

  it "thumbnail scale down image to be exactly 75 by 75 pixels" do
    expect(@uploader.thumbnail).to have_dimensions(75, 75)
  end
end