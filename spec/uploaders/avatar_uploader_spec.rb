require 'spec_helper'
require 'carrierwave/test/matchers'

describe AvatarUploader do

  include CarrierWave::Test::Matchers

  let!(:profile) { create :profile }

  before do
    image_path = "#{::Rails.root}/public/defaults/wave.png"
    AvatarUploader.enable_processing = true
    @uploader = AvatarUploader.new(profile, :avatar)
    @uploader.store!(File.open(image_path))
  end

  after do
    AvatarUploader.enable_processing = false
    @uploader.remove!
  end

  it "should make the image readable only to the owner and not executable" do
    expect(@uploader).to have_permissions(0777)
  end

  it "thumbnail scale down image to fit within 280 by 280 pixels" do
    expect(@uploader.thumbnail).to be_no_larger_than(280, 280)
  end

  it "mini_thumbnail scale down image to fit within 64 by 64 pixels" do
    expect(@uploader.mini_thumbnail).to be_no_larger_than(64, 64)
  end

  it "mini_thumbnail scale down image to be exactly 64 by 64 pixels" do
    expect(@uploader.mini_thumbnail).to have_dimensions(64, 64)
  end
end