require 'spec_helper'
require 'carrierwave/test/matchers'

describe ImageCategoryUploader do

  include CarrierWave::Test::Matchers

  let!(:category) { create :category }

  before do
    image_path = "#{::Rails.root}/public/defaults/wave.png"
    ImageCategoryUploader.enable_processing = true
    @uploader = ImageCategoryUploader.new(category, :image_category)
    @uploader.store!(File.open(image_path))
  end

  after do
    ImageCategoryUploader.enable_processing = false
    @uploader.remove!
  end

  it "should make the image readable only to the owner and not executable" do
    expect(@uploader).to have_permissions(0777)
  end

  it "should scale down image to fit within 200 by 200 pixels" do
    expect(@uploader).to be_no_larger_than(160, 120)
  end

  it "should scale down image to be exactly 160 by 120 pixels" do
    expect(@uploader).to have_dimensions(160, 120)
  end

end