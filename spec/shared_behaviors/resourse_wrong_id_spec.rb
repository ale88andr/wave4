require 'spec_helper'

shared_examples_for "wrong resourse id" do |resourse|

  it { expect{ visit "/admin/#{resourse}/id_0911" }.to raise_exception ActiveRecord::RecordNotFound }

end