require 'spec_helper'

shared_examples_for "default action behavior" do |action_partial|

  it { expect(subject).to be_success }

  # change template if use other partial
  it { expect(subject).to render_template(action_partial) }

end