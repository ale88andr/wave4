require 'spec_helper'

shared_examples_for "active admin comments" do

  it { expect(page).to have_selector 'form#new_active_admin_comment' }

end