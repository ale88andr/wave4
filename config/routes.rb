M2s::Application.routes.draw do

  devise_for :admin_users, ActiveAdmin::Devise.config

  ActiveAdmin.routes(self)

  devise_for :users, path: '/', path_names: { sign_in: 'signin', sign_up: 'signup', sign_out: 'signout' }, controllers: { registrations: "store/visitors/registrations" }

  namespace :store do
    scope module: 'products' do
      get "dashboard/index"
      root 'dashboard#index'

      resources :categories, only: [:index, :show] do
        resources :entities, only: :show
      end
    end

    scope module: 'visitors' do
      resources :users, only: :show do
        resource :profile, except: [:index, :destroy]
      end
    end

    scope module: 'orders' do
      resources :cart_items, only: [:create, :destroy]
      resources :carts do
        get   'empty',                on: :collection
        get   'confirmation',         on: :member
        put   'create_confirmation',  on: :member
      end
    end
  end
  root to: 'store/products/dashboard#index'
end
