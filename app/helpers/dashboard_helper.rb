module DashboardHelper
  def get_all_active_categories
    Category.parental.active
  end

  def cart_size cart
  	CartItem.where(cart_id: cart).count || 0
  end
end
