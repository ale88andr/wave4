module PriceHelper
  def readable_price price, currency = false
    result = price.to_s(:rounded, precision: 2)
    return currency ? result + ' ' + Currency.main.abbreviation : result
  end
end