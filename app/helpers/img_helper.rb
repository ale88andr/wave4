module ImgHelper

	DEFAULT_USER_IMG    	= '/defaults/default_user_avatar.png'
  DEFAULT_PRODUCT_IMG 	= '/defaults/product_nothumb_large.png'
  DEFAULT_CATEGORY_IMG 	= '/defaults/wave.png'

  def rescue_user_avatar(path)
    path.blank? ? DEFAULT_USER_IMG : path
  end

  def image img, title = nil, alt = nil
  	image_tag img, alt: alt, title: title
  end

  def product_cover(img, title = nil, alt = nil)
    image img ||= DEFAULT_PRODUCT_IMG, title, alt
  end

  def category_cover(img, title = nil, alt = nil)
  	image img ||= DEFAULT_CATEGORY_IMG, title, alt
  end
end