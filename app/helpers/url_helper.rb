module UrlHelper
  def index_url controller
    url_for(controller: controller)
  end

  def edit_url resource
    url_for(controller: resource.ctrl_name, action: :edit, id: resource)
  end

  def show_url resource
    url_for(controller: resource.ctrl_name, action: :show, id: resource)
  end
end