module ApplicationHelper

  DEFAULT_APP_NAME = 'Wave'

  def resource_name
    :user
  end

  def resource
    @resource ||= User.new
  end

  def devise_mapping
    @devise_mapping ||= Devise.mappings[:user]
  end

  def set_title title=nil
    content_for?(:title) ? DEFAULT_APP_NAME + " -> " + title : DEFAULT_APP_NAME
  end

  def yes_or_no(param)
    param.nil? ? 'Нет' : 'Да'
  end

  def gender(param)
    param.blank? ? 'Мужской' : 'Женский'
  end

  def rescue_address(address)
    "г. #{address[:city]} , ул. #{address[:street]}" unless address.empty?
  end

  def rescue_empty_value(value)
    value.present? ? value : 'ПУСТО'
  end

  def product_availability(count)
    case count
      when 0..10      then content_tag(:div, "Товар скоро закончится!", class: "text-danger centered")
      when 10..20     then content_tag(:div, "Товар на складе в ограниченном количестве!", class: "text-info centered")
      when 20..1000   then content_tag(:div, "Товар в наличии на складе!", class: "text-success centered")
    end
  end
end
