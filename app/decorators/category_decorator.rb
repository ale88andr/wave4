class CategoryDecorator
  attr_reader :category

  def initialize(category)
    @category = category
  end

  def entities_size
    category.entities.size
  end

  def main?
    category.parent_category.present?
  end

  def main_category_title
    category.parent_category.title
  end

  def subcategories_size
    category.subcategories.size
  end

  def subcategories?
    category.subcategories.present?
  end

  def entities?
    category.entities.present?
  end

  def method_missing(method_name, *args, &block)
    category.send(method_name, *args, &block)
  end

  def respond_to_missing?(method_name, include_private = false)
    category.respond_to?(method_name, include_private) || super
  end
end