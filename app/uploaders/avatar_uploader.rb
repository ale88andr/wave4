# encoding: utf-8

class AvatarUploader < BaseUploader

  version :thumbnail do
    process resize_to_fit: [280, 280]
  end

  version :mini_thumbnail, from_version: :thumbnail do
    process resize_to_fill: [64, 64]
  end
end