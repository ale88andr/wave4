# encoding: utf-8

class EntityUploader < BaseUploader

	version :large_preview do
		process resize_to_fit: [320, 240]
	end

  version :preview do
    process resize_to_fit: [160, 120]
  end

  version :thumbnail, from_version: :preview do
    process resize_to_fill: [75, 75]
  end
end