# encoding: utf-8

class ManufacturerUploader < BaseUploader

  process :resize_to_fit => [150, 150]

  version :min do
    process resize_to_fit: [50, 50]
  end

end
