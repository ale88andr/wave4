# encoding: utf-8

class FeatureUploader < BaseUploader

  process :resize_to_fit => [45, 45]

end