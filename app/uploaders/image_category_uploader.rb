# encoding: utf-8

class ImageCategoryUploader < BaseUploader

  process :resize_to_fit => [160, 120]

end
