class Entity < ActiveRecord::Base
  include DataHandler

  # Association macros
  # -----------------------------------------------------------
  belongs_to :category
  belongs_to :manufacturer
  belongs_to :currency

  has_and_belongs_to_many :features
  
  has_many :entity_properties, class_name: 'Attribute', through: :parameters
  has_many :cart_items

  with_options dependent: :destroy do |association|
    association.has_many :parameters
  end

  accepts_nested_attributes_for :parameters

  # Validation macros
  # -----------------------------------------------------------
  validates :title,
            presence:     true,
            length:       { maximum: 255 },
            uniqueness:   { case_sensitive: false }
  validates :price,
            numericality: { greater_than_or_equal_to: 0 }
  validates :availability, :guarantee,
            numericality: { greater_than_or_equal_to: 0, only_integer: true },
            allow_blank:  true

  # Aliases
  # -----------------------------------------------------------
  delegate :properties,             to: :category, prefix: true
  delegate :parent_category,        to: :category, allow_nil: true
  delegate :abbreviation, :emblem,  to: :currency, prefix: true

  # Callbacks
  # -----------------------------------------------------------
  before_save     :set_characteristics_from_parameters_attributes
  before_create   :input_data_handle, on: :create
  before_destroy  :check_reference_any_cart_item

  # Scopes
  # -----------------------------------------------------------
  scope :published,       -> { where(published: true) }
  scope :unpublished,     -> { where(published: false) }
  scope :expensive,       -> { published.unscope(:order).order('price DESC') }
  scope :cheap,           -> { published.unscope(:order).order('price') }

  # CarrierWave
  # -----------------------------------------------------------
  mount_uploader :cover, EntityUploader

  def to_param
    # "#{id}-#{title.parametrize}"
  end

  def save(*)
    set_product_currency
    super
  end

  # Class methods
  # -----------------------------------------------------------
  protected

    def set_characteristics_from_parameters_attributes
      self.characteristics = self.parameters.map { |p| p.value.to_s + p.attribute.unit.try(:param).to_s }.join(" / ")
    end

    def input_data_handle
      self.title = self.title.squish.truncate(255)
      self.description = safe_txt self.description
    end

    def set_product_currency
      self.currency_id ||= Currency.main
    end

    def check_reference_any_cart_item
      unless cart_items.empty?
        errors.add(:base, 'Cуществуют товарные позиции c этим товаром')
        return false
      end
    end
end
