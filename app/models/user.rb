class User < ActiveRecord::Base

  USERS_GENDER = { true: 'Мужской', false: 'Женский' }
  VALID_EMAIL_FORMAT = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i

  attr_accessor :login

  # Association macros
  # -----------------------------------------------------------
  has_one :profile, dependent: :destroy

  accepts_nested_attributes_for :profile, allow_destroy: true, reject_if: :all_blank

  # Validation macros
  # -----------------------------------------------------------
  validates :name,
            length:       { maximum: 50 },
            presence:     true,
            uniqueness:   { case_sensitive: false }
  validates :email,
            # allow_blank: true,
            presence:     true,
            uniqueness:   { case_sensitive: false },
            format:       { with: VALID_EMAIL_FORMAT }
  validates :password,
            length:       { minimum: 6 },
            presence:     true,
            on:           :create,
            confirmation: true

  # Devise
  # -----------------------------------------------------------
  devise  :database_authenticatable,
          :registerable,
          :recoverable,
          :rememberable,
          :trackable

  # Aliases
  # -----------------------------------------------------------
  delegate  :username,
            :phone,
            :skype,
            :url,
            :gender,
            :birthday,
            :timezone,
            :about,
            :address,
            to: :profile,
            prefix: true,
            allow_nil: true

  delegate :email_visible, to: :profile, allow_nil: true, prefix: false

  def self.find_first_by_auth_conditions(warden_conditions)
    conditions = warden_conditions.dup
    if login = conditions.delete(:login)
      where(conditions).where(["lower(name) = :value OR lower(email) = :value", { :value => login.downcase }]).first
    else
      where(conditions).first
    end
  end

end
