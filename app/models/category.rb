class Category < ActiveRecord::Base
  include DataHandler

  PROHIBITED_CHARACHTERS = /\A[[:alpha:] ]+\Z/

  # Association macros
  # -----------------------------------------------------------
  with_options class_name: "Category", foreign_key: "parent_id" do |association|
    association.has_many    :subcategories
    association.belongs_to  :parent_category, dependent: :destroy
  end

  with_options dependent: :destroy do |association|
    association.has_many  :entities
  end

  has_and_belongs_to_many :properties, class_name: 'Attribute', join_table: :attributes_categories

  # Validation macros
  # -----------------------------------------------------------
  validates :title,
            presence:     true,
            length:       { maximum: 30 },
            uniqueness:   { case_sensitive: false },
            format:       { with: PROHIBITED_CHARACHTERS }
  validates :order,
            numericality: { greater_than_or_equal_to: 0, only_integer: true },
            allow_blank:  true

  # Callbacks
  # -----------------------------------------------------------
  before_save   :input_data_handle, :set_default_category_scale, on: [:create, :update]
  before_create :set_start_order

  # Scopes
  # -----------------------------------------------------------
  scope :parental,  -> { where(parent_id: 0) }
  scope :active,    -> { where(active: true) }
  scope :for_index, -> { active.parental.order('title ASC') }
  scope :subcat,    -> { active.where('parent_id != 0') }

  # CarrierWave
  # -----------------------------------------------------------
  mount_uploader :image_category, ImageCategoryUploader

  protected

    def input_data_handle
      self.title = safe_str self.title
      self.description = safe_txt self.description
    end

    def set_start_order
      self.order = Category.maximum('order').to_i + 1 || 1
    end

    def set_default_category_scale
      self.parent_id = 0 if self.parent_id.nil?
    end

end
