class Manufacturer < ActiveRecord::Base
  include DataHandler

  VALID_URL_FORMAT = /[\w\-_]+(\.[\w\-_]+)+([\w\-\.,@?^=%&amp;:+#]*[\w\-\@?^=%&amp;+#])?/i

  # Association macros
  # -----------------------------------------------------------
  has_many :entities

  # Validation macros
  # -----------------------------------------------------------
  validates :name,
            presence:     true,
            uniqueness:   { case_sensitive: false },
            length:       { maximum: 150 }
  validates :url,
            format:       { with: VALID_URL_FORMAT },
            allow_blank:  true

  # CarrierWave
  # -----------------------------------------------------------
  mount_uploader :thumb, ManufacturerUploader

  # Callbacks
  # -----------------------------------------------------------
  before_save :input_data_handle, on: [:create, :update]

  # Class methods
  # -----------------------------------------------------------
  protected

    def input_data_handle
      self.name = safe_str self.name
      self.description = safe_txt self.description, 500
    end

end
