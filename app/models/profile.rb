class Profile < ActiveRecord::Base

  VALID_URL_FORMAT = /[\w\-_]+(\.[\w\-_]+)+([\w\-\.,@?^=%&amp;:+#]*[\w\-\@?^=%&amp;+#])?/i

  serialize :address, Hash

  # Association macros
  # -----------------------------------------------------------
  belongs_to :user

  # Validation macros
  # -----------------------------------------------------------
  validates_length_of   :username, :phone, :skype,
                        maximum:      25
  validates_format_of   :url,
                        with:         VALID_URL_FORMAT,
                        allow_blank:  true

  # CarrierWave
  # -----------------------------------------------------------
  mount_uploader :avatar, AvatarUploader
end
