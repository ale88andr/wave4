class Unit < ActiveRecord::Base

	# Association macros
  # -----------------------------------------------------------
  has_many :attr, class_name: "Attribute"

  # Validation macros
  # -----------------------------------------------------------
  validates :param,
            presence:   true,
            uniqueness: { case_sensitive: false },
            length:     { maximum: 15 }
end
