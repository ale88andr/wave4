class Feature < ActiveRecord::Base
  include DataHandler

  # Association macros
  # -----------------------------------------------------------
  has_and_belongs_to_many :entities

  # Validation macros
  # -----------------------------------------------------------
  validates :name,
            presence:   true,
            uniqueness: { case_sensitive: false },
            length:     { maximum: 50 }

  # Callbacks
  # -----------------------------------------------------------
  before_save   :input_data_handle

  # CarrierWave
  # -----------------------------------------------------------
  mount_uploader :label, FeatureUploader

  protected

    def input_data_handle
      self.name = safe_str self.name
      self.description = safe_txt self.description
    end

end
