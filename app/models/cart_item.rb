class CartItem < ActiveRecord::Base

  # Association macros
  # -----------------------------------------------------------
  belongs_to :entity
  belongs_to :cart

  # Aliases
  # -----------------------------------------------------------
  delegate :id, :title, :price, :currency, :category_id, to: :entity, prefix: true

  def total_item_price
  	entity.price * quantity
  end
end
