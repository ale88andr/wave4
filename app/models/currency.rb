class Currency < ActiveRecord::Base

  TYPES = {
            UAH: 'Гривна',  RUB: 'Рубль',   USD: 'Доллар',  GBP: 'Фунт Стерлингов',
            EUR: 'Евро',    JPY: 'Иена',    ILS: 'Шекель',  INR: 'Рупия',
            KRW: 'Вона',    NGN: 'Наира',   THB: 'Бат',     VND: 'Донг',
            LAK: 'Кип',     KHR: 'Риель',   MNT: 'Тугрик',  PHP: 'Песо',
            CRC: 'Колон',   PYG: 'Гуарани', AFN: 'Афгани',  GHS: 'Седи'
          }

  DESCRIPTORS = { NO: 'Не задействованна', PRIMARY: 'Основная', SECONDARY: 'Второстепенная' }

  attr_accessor :descriptor

  # Association macros
  # -----------------------------------------------------------
  has_many :entities

  # Validation macros
  # -----------------------------------------------------------
  validates :name,
            presence:     true,
            uniqueness:   { case_sensitive: false },
            inclusion:    { in: TYPES.values }
  validates :ratio,
            numericality: { greater_than_or_equal_to: 0 },
            allow_blank:  true

  # CarrierWave
  # -----------------------------------------------------------
  mount_uploader :emblem, CurrencyUploader

  # Callbacks
  # -----------------------------------------------------------
  before_save       :set_currency_abbreviation, on: :create
  before_validation :set_currency_descriptor

  # Scopes
  # -----------------------------------------------------------
  scope :main,       -> { where(prime_descriptor: true).first }
  scope :secondary,  -> { where(sec_descriptor: true).first }

  protected

    def set_currency_abbreviation
      self.abbreviation = TYPES.key(self.name)
    end

    def set_currency_descriptor
      case self.descriptor
        when DESCRIPTORS[:NO]         then set_no_description
        when DESCRIPTORS[:PRIMARY]    then set_primary_description
        when DESCRIPTORS[:SECONDARY]  then set_secondary_description
        else set_no_description
      end
    end

    def set_no_description
      self.prime_descriptor, self.sec_descriptor = false, false
    end

    def set_primary_description
      Currency.update_all(prime_descriptor: false)
      self.prime_descriptor = true
      self.ratio = 1
    end

    def set_secondary_description
      Currency.update_all(sec_descriptor: false)
      self.sec_descriptor = true
    end
end
