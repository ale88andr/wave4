class Attribute < ActiveRecord::Base
  include DataHandler

  VALID_ATTRIBUTE_NAME_FORMAT =  /\A[[:word:] ]+\Z/

  # Association macros
  # -----------------------------------------------------------
  belongs_to :unit

  has_many :parameters
  has_many :entities, through: :parameters

  has_and_belongs_to_many :categories

  # Validation macros
  # -----------------------------------------------------------
  validates :name,
            presence:   true,
            uniqueness: { case_sensitive: false },
            length:     { maximum: 25 },
            format:     { with: VALID_ATTRIBUTE_NAME_FORMAT }

  # Callbacks
  # -----------------------------------------------------------
  before_create :input_data_handle, on: :create

  # Instance methods
  # -----------------------------------------------------------
  private

    def input_data_handle
      self.name = safe_str self.name
    end
end
