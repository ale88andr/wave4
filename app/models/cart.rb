class Cart < ActiveRecord::Base

  VALID_USERNAME_FORMAT = /[[:alpha:]. -]+\z/i
  VALID_PHONE_FORMAT = /[\d()-]+/i

  serialize :cart_info, Hash

  # Association macros
  # -----------------------------------------------------------
  has_many :cart_items, dependent: :destroy

  # Validation macros
  # -----------------------------------------------------------

  validate  :cart_info_validate_username,
            :cart_info_validate_phone,
            :cart_info_validate_street,
            on: :update

  validate :cart_info_validate_true_username, if: lambda{ cart_info[:username].present? }
  validate :cart_info_validate_true_phone,    if: lambda{ cart_info[:phone].present? }

  def is_hash?
    cart_info.is_a(Hash)
  end

  def cart_info_validate_username
    presence_validate cart_info[:username], I18n.t('.carts.fields.ci_username')
  end

  def cart_info_validate_phone
    presence_validate cart_info[:phone], I18n.t('.carts.fields.ci_phone')
  end

  def cart_info_validate_street
    presence_validate cart_info[:street], I18n.t('.carts.fields.ci_street')
  end

  def cart_info_validate_true_username
    unless cart_info[:username] =~ VALID_USERNAME_FORMAT
      errors.add(I18n.t('.carts.fields.ci_username'), I18n.t('.carts.error_messages.ci_username_real'))
    end
  end

  def cart_info_validate_true_phone
    unless cart_info[:phone] =~ VALID_PHONE_FORMAT
      errors.add(I18n.t('.carts.fields.ci_phone'), I18n.t('.carts.error_messages.ci_phone_real'))     
    end
  end

  def presence_validate data, field_name = ''
    if data.try(:squish).blank?
      errors.add(field_name, I18n.t('.carts.error_messages.ci_presence'))
    end
  end

  # Scopes
  # -----------------------------------------------------------
  scope :confirmed_by_user, ->{ where(user_confirmation:true) }
  scope :preorders,         ->{ confirmed_by_user.where(admin_user_confirmation: false) }
  scope :orders,            ->{ confirmed_by_user.where(admin_user_confirmation: true) }
  scope :empty_orders,      ->{ where("user_confirmation = ? AND created_at = ?",
                                      false, Time.now.midnight - 1.day) }

  # Instance methods
  # -----------------------------------------------------------
  def add_product(product_id)
    current_item = cart_items.find_by(entity_id: product_id)
    if current_item
      current_item.increment!(:quantity)
    else
      current_item = cart_items.build(entity_id: product_id)
    end
    current_item
  end

  def total_cart_price
    cart_items.to_a.sum { |item| item.total_item_price }
  end
end
