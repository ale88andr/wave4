class Entities::ShowPresenter
  def initialize(entity)
    @product = entity
  end

  def product
    @product
  end

  def parent?
    @product.parent_category.present?
  end

  def main_category
    @product.parent_category
  end

  def sub_category
    @product.category
  end

  def related_categories
    main_category.subcategories
  end
end