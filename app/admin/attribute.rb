ActiveAdmin.register Attribute do

  config.per_page = 15

  menu  priority: 12,
        label:    'Аттрибуты товаров',
        parent:   'Меню товаров'

  permit_params :name, :unit_id

  filter  :categories,
          collection: Category.subcat,
          label: 'Категория'
  filter  :name,
          label: 'Наименование параметра'
  filter  :unit,
          as: :select,
          collection: Hash[Unit.all.map{ |u| [u.param,u.id] }],
          label: 'Единица измерения'

  form do |f|
    f.inputs t('.attribute_title_name') do
      f.input :name,
              label:        t('.attribute_name'),
              hint:         t('.attribute_name_hint'),
              input_html:   { maxlength: 25 }
    end

    f.inputs t('.attribute_title_unit') do
      f.input :unit_id,
              as:             :select,
              collection:     Hash[Unit.all.map{ |u| [u.param,u.id] }],
              label:          t('.attribute_unit'),
              hint:           t('.attribute_unit_hint'),
              include_blank:  t('.attribute_no_unit')
    end

    f.actions
  end

  index title: 'Аттрибуты товаров' do
    selectable_column
    column t('.attribute_name'), sortable: :name do |attr|
      link_to attr.name, admin_attribute_path(attr)
    end

    column t('.attribute_unit') do |attr|
      status_tag (attr.unit.blank? ? t('.attributes_no_unit') : attr.unit.param), (attr.unit.blank? ? :error : :ok )
    end
    
    column t('.attribute_category') do |attr|
      status_tag (attr.categories.empty? ? t('.attributes_no_category') : attr.categories.collect{|c| c.title}.join(', ')), (attr.categories.empty? ? :error : :ok )
    end

    default_actions
  end

  show do
    panel t('.attribute_panel_title') + "#{attribute.name}" do
      attributes_table_for attribute do
        row(t('.attribute_name'))     { attribute.name }
        row(t('.attribute_unit'))     { status_tag(attribute.unit.blank? ? t('.attributes_no_unit') : attribute.unit.param) }
        row(t('.attribute_category')) { status_tag(attribute.categories.empty? ? t('.attributes_no_category') : attribute.categories.collect{|c| c.title}.join(', ')) }
      end
    end

    active_admin_comments
  end

  sidebar 'Категории: ', only: [:show, :index] do
    table_for Category.all do |c|
      c.column('Категория :') { |cat| link_to cat.title, admin_category_path(cat) }
      c.column('Иерархия :')  { |cat| status_tag(cat.parent_id == 0 ? 'Родительская' : cat.parent_category.title, cat.parent_category.blank? ? :ok : :error) }
    end
  end
end
