ActiveAdmin.register Entity do

  # Pagination index page
  config.per_page = 25

  # Menu display index
  menu label: 'Товары', parent: 'Меню товаров'

  # Set filters
  filter :title,        label: 'Название продукта:'
  filter :manufacturer, label: 'Производитель:'
  filter :price,        label: 'Цена:'
  filter :created_at,   label: 'Дата добавления:'

  # Scopes
  scope :all, default: true
  scope :published
  scope :unpublished
  scope :expensive
  scope :cheap

  # Permission
  permit_params :title, 
                :manufacturer_id, 
                :description, 
                :price,
                :price_end_date, 
                :price_exchange, 
                :availability,
                :guarantee,
                :published,
                :cover,
                # { advise: [] },
                { feature_ids: [] },
                { parameters_attributes: [:attribute_id, :value] }

  # Assosiation
  belongs_to :category, optional: true

  # Controller extension
  controller do
    def new
      @entity = Entity.new(category_id: params[:category_id])
      @entity.parameters.build
      @category = Category.find_by_id(params[:category_id])
    end
  end

  # New/Edit forms
  form do |f|
    f.inputs t('.entity_form_title') do
      # f.input :category,
      #         required:       true,
      #         as:             :select,
      #         collection:     Hash[Category.subcat.map{ |c| [c.title,c.id] }],
      #         include_blank:  false,
      #         label:          t('.entity_category'),
      #         hint:           t('.entity_category_hint')
      f.input :manufacturer,
              required:       true,
              as:             :select,
              collection:     Hash[Manufacturer.all.map{ |m| [m.name,m.id] }],
              include_blank:  false,
              label:          t('.entity_manufacturer'),
              hint:           t('.entity_manufacturer_hint')
      f.input :title,
              required:       true,
              label:          t('.entity_title'),
              hint:           t('.entity_title_hint')
      f.input :description,
              as:             :text,
              rows:           10,
              cols:           20,
              maxlength:      1500,
              label:          t('.entity_desc'),
              hint:           t('.entity_desc_hint')
      f.input :price,
              required:       true,
              as:             :number,
              step:           0.2,
              min:            0,
              input_html:     { size: 10 },
              label:          t('.entity_price') + " (#{Currency.main.name})",
              hint:           t('.entity_price_hint')
      f.input :price_end_date,
              label:          t('.entity_price_end_date'),
              hint:           t('.entity_price_end_date_hint'),
              as:             :date_picker,
              class:          'datepicker'
      f.input :price_exchange,
              label:          t('.entity_price_exchange') + " (#{Currency.secondary.name})"
      f.input :availability,
              as:             :number,
              step:           1,
              min:            0,
              input_html:     { size: 5 },
              label:          t('.entity_availability'),
              hint:           t('.entity_availability_hint')
      f.input :guarantee,
              as:             :number,
              step:           1,
              min:            0,
              input_html:     { size: 5 },
              label:          t('.entity_guarantee'),
              hint:           t('.entity_guarantee_hint')
      f.input :advise,
              as:             :check_boxes,
              collection:     Hash[],
              include_blank:  true,
              label:          t('.entity_advise'),
              hint:           t('.entity_advise_hint')
      f.input :published,
              checked:        :true,
              label:          t('.entity_published')
      f.input :cover,
              as:             :file,
              label:          t('.entity_cover'),
              hint:           t('.entity_cover_hint')
      f.input :features,
              as:             :check_boxes,
              value_method:   :name,
              collection:     Hash[Feature.all.map{ |m| [image_tag(m.label.url, size: "25x25"),m.id] }],
              label:          t('.entity_feature'),
              hint:           t('.entity_feature_hint')
      f.inputs t('.entities_parameters'), class: 'basic' do
        f.template.render partial: "parameters", locals: {category: entity.category, form: f}
      end
    end

    f.actions
  end

  # Show page
  show do
    panel t('.show_entity_details_panel_title') + "'#{entity.category.title}'" do
      render partial: "show_details", locals: { entity: entity }
    end

    panel t('.show_entity_references_panel_title') + "'#{entity.title}'" do
      render partial: "show_references", locals: { params: entity.parameters }
    end

    active_admin_comments
  end

  # Index page
  index do |entity|
    div do
      span b link_to(entity.category.parent_category.title, admin_category_path(entity.category.parent_category))
      span "/ "
      span link_to(entity.category.title, admin_category_path(entity.category))
      br
    end

    selectable_column

    column t('.entity_title'), sortable: :title do |e|
      b { link_to e.title, admin_category_entity_path(e.category_id, e.id) }
    end

    column t('.entity_manufacturer') do |e|
      e.manufacturer.try(:name) ? link_to( e.manufacturer.name, admin_manufacturer_path(e.manufacturer) ) : 'Недоступно'
    end

    column t('.entity_price'), sortable: :price do |e|
      e.price.nil? ? 'Недоступна' : e.price.to_s(:rounded, precision: 2)
    end

    column t('.entity_created'), :created_at

    # default_actions
  end

  # Sidebars
  sidebar 'Ещё из этой категории: ', only: :show do
    table_for entity.category do |c|
      c.column('Категория') { |cat| link_to cat.title, admin_category_path(cat.id) }
      c.column('Статус')    { |cat| status_tag(cat.active ? 'Активна' : 'Не активна', cat.active ? :ok : :error) }
    end
  end

  sidebar 'Товары категории :', only: :show do
    table_for Category.find_by_id(entity.category).entities do |e|
      e.column('Наименование :')  { |ent| link_to ent.title, admin_category_entity_path(entity.category_id, ent.id) }
      e.column('Цена :')          { |ent| status_tag(ent.price.to_s, ent.price ? :ok : :error) }
    end
  end

  sidebar 'Ещё подкатегории:', only: :index do
    table_for entities.first.category.parent_category.subcategories do |c|
      c.column('Наименование :')  { |subc| link_to subc.title, admin_category_path(subc.id) }
      c.column('Товаров :')       { |subc| b subc.entities.count }
    end
  end

  # Additional widget
  action_item only: :show do
    link_to "Все товары этой категории *", admin_category_entities_path(entity.category_id)
  end
end
