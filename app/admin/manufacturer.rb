ActiveAdmin.register Manufacturer do

  # Pagination index page
  config.per_page = 15

  # Menu display index
  menu  priority: 10,
        label:    'Производители',
        parent:   'Меню товаров'

  # Cancel actions
  actions :all, except: [:show]

  # Permission
  permit_params :name, :description, :url, :thumb

  # Filters
  filter :name, label: 'Название'
  filter :created_at, label: 'Дата создания'

  # New/Edit forms
  form do |f|
    f.inputs t('.manufacturer_form_title') do
      f.input :name,
              input_html:     { maxlength: 150 },
              label:          t('.manufacturer_title'),
              hint:           t('.manufacturer_title_hint')
      f.input :description,
              as:             :text,
              input_html:     { rows: 10, cols: 20 },
              label:          t('.manufacturer_description'),
              hint:           t('.manufacturer_description_hint')
      f.input :url,
              as:             :url,
              label:          t('.manufacturer_url'),
              hint:           t('.manufacturer_url_hint')
      f.input :thumb,
              as:             :file,
              label:          t('.manufacturer_thumb'),
              hint:           f.object.thumb.present? ? f.template.image_tag(f.object.thumb.url) : f.template.content_tag(:span, t('.manufacturer_thumb_hint'))
      f.input :thumb_cache,
              :as => :hidden
    end

    f.actions
  end

  # Index page
  index do
    column t('.manufacturer_thumb') do |m|
      image_tag m.thumb.url
    end
    column t('.manufacturer_name'), sortable: :name do |m|
      link_to m.name, m.url
    end
    column t('.manufacturer_description'), :description
    column t('.manufacturer_created_at'), :created_at
  
    default_actions
  end
end
