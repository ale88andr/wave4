ActiveAdmin.register Cart do

  # Pagination index page
  config.per_page = 15

  # Menu display index
  menu label: proc{ I18n.t('.menu.cart_label') }

  # Cancel actions
  actions :all, except: [:create,:new]

  # Set filters
  filter  :id,
          label: I18n.t('.filters.cart_id')
  filter  :user_confirmation,
          label: I18n.t('.filters.cart_user_confirmed')
  filter  :admin_user_confirmation,
          label: I18n.t('.filters.cart_admin_confirmed')
  filter  :created_at,
          label: I18n.t('.filters.cart_created')
  filter  :cart_info,
          as: :string,
          label: I18n.t('.filters.cart_info')

  # Scopes
  scope :confirmed_by_user, default: true
  scope :preorders
  scope :orders
  scope :empty_orders

  # Actions
  member_action :confirm, method: :put do
    @cart = Cart.find(params[:id])
    if @cart.user_confirmation && @cart.update_column(:admin_user_confirmation, true)
      redirect_to action: :index, notice: I18n.t('.confirm.confirmed')
    else
      redirect_to action: :show, error: I18n.t('.confirm.not_confirmed')
    end
  end

  # Index page
  index do |cart|
    selectable_column

    column t('.cart_id'), sortable: :id do |c|
      b { link_to "##{c.id}", admin_cart_path(c) }
    end

    column t('.cart_created'), :created_at

    column t('.cart_user_confirmed'), sortable: false do |c|
      status_tag (c.user_confirmation.blank? ? 'Нет' : 'Да'),
      c.user_confirmation.blank? ? :error : :ok
    end

    column t('.cart_admin_user_confirmed'), sortable: false do |c|
      status_tag (c.admin_user_confirmation.blank? ? 'Нет' : 'Да'),
      c.admin_user_confirmation.blank? ? :error : :ok
    end

    column t('.cart_items_sum') do |c|
      status_tag (readable_price c.total_cart_price), :ok
    end

    column t('.cart_info') do |c|
      b c.cart_info[:username] + ', ' + c.cart_info[:phone]
    end

    default_actions
  end

  # Show page
  show do
    panel t('.show_cart_panel_title') + "#{cart.id}" do
      h3 t('.cart_id') + " ##{cart.id}"
      hr
      table_for cart.cart_items do |t|
        t.column(t('.cart_item_quantity'))  {
          |ci| "#{ci.quantity} x"
        }
        t.column(t('.cart_item_entity'))    {
          |ci|  link_to ci.entity.title,
                category_entity_path(ci.entity_category_id, ci.entity_id)
        }
        t.column(t('.cart_item_price'))     {
          |ci| b  readable_price(ci.total_item_price) +
                  ' ' + ci.entity.currency_abbreviation
        }
        t.column(t('.cart_item_ctrl'))      {
          |ci| link_to 'Удалить', ci, method: :delete 
        }
        tr class: 'odd' do
          td
          td t('.cart_items_total'), style: 'text-align: right;'
          td b(readable_price(cart.total_cart_price))
          td
        end
      end
      attributes_table do
        row(t('.cart_customer_name'))       { b cart.cart_info[:username] }
        row(t('.cart_customer_phone'))      { b cart.cart_info[:phone] }
        row(t('.cart_customer_adress'))     {
          b cart.cart_info[:city] + ', ' + cart.cart_info[:street]
        }
        row(t('.cart_customer_price'))      {
          h2 readable_price(cart.total_cart_price)
        }
        row(t('.cart_customer_confirm'))    {
          status_tag (cart.user_confirmation? ? 'Да' : 'Нет'),
          cart.user_confirmation? ? :ok : :error
        }
        row(t('.cart_admin_confirm'))       {
          status_tag (cart.admin_user_confirmation? ? 'Да' : 'Нет'),
          cart.admin_user_confirmation? ? :ok : :error
        }
        row(t('.cart_created'))             {
          cart.created_at? ? l(cart.created_at, :format => :long) : '-'
        }
      end
    end
  end

  # Action links
  action_item only: :show do
    link_to( 'Подтвердить', confirm_admin_cart_path(cart), method: :put ) if cart.user_confirmation
  end
end