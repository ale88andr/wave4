ActiveAdmin.register Unit do

  # Menu display index
  menu  priority: 13,
        label:    'Единицы аттрибутов',
        parent:   'Меню товаров'

  # Cancel actions
  actions :all, except: [:show]

  # Permission
  permit_params :param

  # New/Edit forms
  form do |f|
    f.inputs 'Новая единица измерения' do
      f.input   :param,
                label:  t('.unit_param'),
                hint:   t('.unit_hint'),
                input_html: { maxlength: 15 }
    end

    f.actions
  end

  # Index page
  index title: 'Единицы измерения товаров' do
    selectable_column
    column t('.unit_param'),   :param
    column t('.unit_attribute') do |unit|
      status_tag  (unit.attr_ids.empty? ? t('.unit_attributes_no') : unit.attr.collect{|attr| attr.name}.join(', ')),
                  (unit.attr_ids.empty? ? :error : :ok )
    end

    default_actions
  end
end
