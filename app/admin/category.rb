ActiveAdmin.register Category do

  # Pagination index page
  config.per_page = 15

  # Menu display index
  menu  priority: 2,
        label:    'Категории товаров',
        parent:   'Меню товаров'

  # Permission
  permit_params :title,
                :parent_id,
                :description,
                :active,
                :order,
                :image_category,
                { property_ids: [] }

  # Filters
  filter  :parent_category,
          as:           :select,
          collection:   Category.parental,
          label:        'Категории'
  filter  :title,       label: 'Название категории'
  filter  :active,      label: 'Доступность категории'
  filter  :created_at,  label: 'Дата создания'

  # Scopes
  scope :all, default: true
  scope :active
  scope :parental
  scope :subcat

  # New/Edit forms
  form do |f|
    f.inputs 'Параметры новой категории' do
      f.input :title,
              input_html:     { maxlength: 30 },
              label:          t('.category_title'),
              hint:           t('.category_title_hint')
      f.input :parent_id,
              as:             :select,
              collection:     Hash[Category.parental.map{ |c| [c.title,c.id] }],
              include_blank:  t('.category_is_parent'),
              label:          t('.category_def_scale'),
              hint:           t('.category_def_scale_hint')
      f.input :description,
              as:             :text,
              input_html:     { :rows => 10, :cols => 20, :maxlength => 300 },
              label:          t('.category_description'),
              hint:           t('.category_description_hint')
      f.input :active,
              as:             :select,
              collection:     [[t('.category_active_enable'), true], [t('.category_active_disable'), false]],
              include_blank:  false,
              label:          t('.category_active'),
              hint:           t('.category_active_hint')
      f.input :properties,
              as:             :check_boxes,
              include_blank:  false,
              label:          t('.category_attributes'),
              hint:           t('.category_attributes_hint')
      f.input :image_category,
              as:             :file,
              label:          t('.category_image'),
              hint:           f.object.image_category.present? ? f.template.image_tag(f.object.image_category.url) : f.template.content_tag(:span, t('.category_image_hint'))
      f.input :image_category_cache,
              :as => :hidden
    end

    f.actions
  end

  # Index page
  index title: 'Категории товаров' do
    selectable_column
    column t('.category_order'), max_width: "50px", sortable: :order do |c|
      b c.order
    end
    column t('.category_title'), sortable: :title do |category|
      link_to category.title, admin_category_path(category)
    end
    column t('.category_parent') do |category|
      status_tag (category.parent_id == 0 ? 'Родительская' : category.parent_category.try(:title)), category.parent_category.blank? ? :ok : :error
    end  
    column t('.category_created'), :created_at

    default_actions
  end

  # Show page
  show do
    panel t('.show_category_panel_title') + "#{category.title}" do
      attributes_table_for category do
        row(t('.category_title'))       { b category.title }
        row(t('.category_description')) { category.description if category.description? }
        row(t('.category_attributes'))  { status_tag (category.properties.empty? ? 'Не назначенны' : category.properties.collect{ |p| p.name }.join(', ')), (category.properties.empty? ? :error : :ok) }
        row(t('.category_active'))      { status_tag (category.active ? "Доступна" : "Недоступна"), (category.active ? :ok : :error) }
        row(t('.category_scale'))       { category.parent_id == 0 ? raw(category.subcategories.collect{ |sc| (link_to sc.title, sc) }.join(', ')) : raw(link_to category.parent_category.title, category.parent_category) }
        row(t('.category_order'))       { category.order if category.order? }
        row(t('.category_image'))       { image_tag(category.image_category_url) }
        row(t('.category_created'))     { category.created_at? ? l(category.created_at, :format => :long) : '-' }
      end
    end

    active_admin_comments
  end

  # Additional widget
  action_item only: :show do
    if category.parent_id > 0
      link_to "Создать товар в этой категории *", new_admin_category_entity_path(category)
    end
  end

  action_item only: :show do
    if category.parent_id > 0
      link_to "Товары категории", admin_category_entities_path(category)
    end
  end

  # Sidebars
  sidebar 'Ещё из этой категории: ', only: :show, if: proc{ category.parent_category } do
    table_for category.parent_category.subcategories do |c|
      c.column('Категория') { |cat| link_to cat.title, admin_category_path(cat) }
      c.column('Статус')    { |cat| status_tag(cat.active ? 'Активна' : 'Не активна', cat.active ? :ok : :error) }
    end
  end
end
