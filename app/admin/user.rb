ActiveAdmin.register User do

  # Menu display index
  menu priority: 1, label: 'Посетители', parent: 'Пользователи'

  # Filters
  filter :name
  filter :email
  filter :last_sign_in_ip

  # Controller extension
  controller do
    def new
      @user = User.new
      @user.build_profile
    end
    
    def edit
      @user = User.find(params[:id])
      if @user.profile.nil?
        @user.build_profile
      end
    end

    def permitted_params
      params.permit!
    end
  end

  # New/Edit forms
  form do |f|
    f.inputs t('.user_account_title') do
      f.input :name,
              label:  t('.user_login'),
              hint:   t('.user_login_hint')
      f.input :email,
              label:  t('.user_email'),
              hint:   t('.user_email_hint')
      f.input :password,
              label:  t('.user_password'),
              required: true,
              hint:   t('.user_password_hint')
      f.input :password_confirmation,
              label:  t('.user_password_confirmation'),
              required: true,
              hint:   t('.user_password_confirmation_hint')
    end
    f.inputs t('.user_account_profile_title') do
      f.semantic_fields_for :profile do |profile|
        profile.inputs do
          profile.input :username,
                        label:  t('.user_name'),
                        hint:   t('.user_name_hint'),
                        required: false
          profile.input :phone,
                        label:  t('.user_phone'),
                        hint:   t('.user_phone_hint')
          profile.input :skype,
                        label:  t('.user_skype'),
                        hint:   t('.user_skype_hint')
          profile.input :url,
                        label:  t('.user_url'),
                        hint:   t('.user_url_hint')
          profile.input :birthday,
                        label:  t('.user_birthday'),
                        hint:   t('.user_birthday_hint'),
                        as:     :date_picker,
                        class:  'datepicker'
          profile.input :email_visible,
                        as:     :boolean,
                        label:  'Показывать мой Email'
          profile.input :about,
                        as:     :text,
                        label:  t('.user_about'),
                        hint:   t('.user_about_hint'),
                        input_html: { :rows => 10, :cols => 20, :maxlength => 300 }
          profile.input :time_zone,
                        as:     :time_zone,
                        label:  t('.user_tz'),
                        hint:   t('.user_tz_hint')
          profile.input :gender,
                        as:     :select,
                        collection: Hash[User::USERS_GENDER.map{ |key, value| [value, key] }],
                        include_blank: t('.user_gender_blank'),
                        label:  t('.user_gender'),
                        hint:   t('.user_gender_hint')
          profile.input :avatar,
                        as:     :file,
                        label:  t('.user_avatar'),
                        hint:   t('.user_avatar_hint')
        end
      end
    end
    f.actions
  end

  # Index page
  index do
    column  t('.user_name'),
            :name,
            sortable: :name
    column  t('.user_email'),
            :email,
            sortable: false
    column  t('.user_last_sign_in_ip'),
            :created_at

    default_actions
  end

  # Show page
  show do
    panel "Пользователь #{user.name} :" do
      attributes_table_for user do
        row(t('.user_login'))     { h2 user.name }
        row(t('.user_name'))      { b user.profile.username if user.profile.present? }
        row(t('.user_email'))     { b user.email }
        row(t('.user_reg_date'))  { user.created_at? ? l(user.created_at, :format => :long) : '-' }
        row(t('.user_email_vis')) { status_tag (user.profile.email_visible? ? "Доступен" : "Недоступен"), (user.profile.email_visible? ? :ok : :error) }
        row(t('.user_birthday'))  { user.profile.birthday? ? l(user.profile.birthday, :format => :long) : '-' }
        row(t('.user_gender'))    { status_tag (user.profile.gender ? "Женский" : "Мужской"), (user.profile.gender ? :ok : :error) }
        row(t('.user_phone'))     { i user.profile.phone if user.profile.present? }
        row(t('.user_skype'))     { user.profile.skype if user.profile.present? }
        row(t('.user_url'))       { link_to(user.profile.url) if user.profile.present? }
        row(t('.user_address'))   { rescue_address user.profile.address }
        row(t('.user_tz'))        { user.profile.time_zone if user.profile.present? }
        row(t('.user_avatar'))    { image_tag(user.profile.avatar.mini_thumbnail) if user.profile.present? }
        row(t('.user_about'))     { i user.profile.about if user.profile.present? }
        row(t('.user_last_visit')){ user.last_sign_in_at? ? l(user.last_sign_in_at, :format => :long) : '-' }
        row(t('.user_last_ip'))   { i user.last_sign_in_ip }
      end
    end

    active_admin_comments
  end
end
