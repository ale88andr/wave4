ActiveAdmin.register AdminUser do

  # Cancel paginate
  config.paginate = false

  # Separation of duty
  if proc{ current_admin_user.super_admin.blank? }
    actions :all, except: [:new, :create, :destroy]
  end

  # Menu display index
  menu  priority: 8,
        label:    'Администраторы',
        parent:   'Пользователи'

  # Filters
  filter :email

  # Controller extension
  controller do
    def permitted_params
      params.permit admin_user: [:email, :password, :password_confirmation]
    end
  end

  # New/Edit forms
  form do |f|
    f.inputs t('.admin_user') do
      f.input :email,
              label:      t('.admin_user_email'),
              hint:       t('.admin_user_email_hint'),
              input_html: { maxlenght: 50 }
      f.input :password,
              label:      t('.admin_user_password'),
              hint:       t('.admin_user_password_hint')
      f.input :password_confirmation,
              required:   true,
              label:      t('.admin_user_password_conf'),
              hint:       t('.admin_user_password_conf_hint')
    end

    if current_admin_user.super_admin?
      f.inputs t('.admin_user_additional') do
        f.input :super_admin,
                label:      t('.admin_user_super'),
                hint:       t('.admin_user_super_hint')
        f.input :blocked,
                label:      t('.admin_user_block'),
                hint:       t('.admin_user_block_hint')
      end
    end

    f.actions
  end

  # Index page
  index download_links: false do
    column t('.admin_user_email'), sortable: :email do |admin|
      link_to admin.email, admin_admin_user_path(admin)
    end
    column t('.admin_user_lsi'), :last_sign_in_at
    column t('.admin_user_sic'), :sign_in_count

    actions defaults: false do |admin|
      current_admin_user == admin || current_admin_user.super_admin? ? link_to(t('.admin_user_actions_edit'), edit_admin_admin_user_path(admin)) : 'НЕ ДОСТУПНО'
    end

    actions defaults: false do |admin|
      current_admin_user.super_admin? ? link_to(t('.admin_user_actions_delete'), admin_admin_user_path(admin), method: :delete) : 'НЕ ДОСТУПНО'
    end
  end
end
