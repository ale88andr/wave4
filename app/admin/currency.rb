ActiveAdmin.register Currency do

  # Menu Item
  menu label: 'Валюта'

  # Set Permission
  permit_params :name, :emblem, :ratio, :display, :descriptor

  # Cancel actions
  actions :all, except: [:show]

  # Filters
  filter :name
  filter :abbreviation
  filter :display

  # Form
  form do |f|
    f.inputs t('.currency_form_title') do
      f.input :name,
              as:             :select,
              collection:     Currency::TYPES.map{ |k,v| v },
              include_blank:  false,
              required:       true,
              label:          t('.currency_name'),
              hint:           t('.currency_name_hint')
      f.input :ratio,
              required:       true,
              as:             :number,
              step:           0.01,
              min:            0,
              input_html:     { size: 5 },
              label:          t('.currency_ratio'),
              hint:           t('.currency_ratio_hint')
      f.input :descriptor,
              as:             :select,
              collection:     Currency::DESCRIPTORS.map{ |k,v| v },
              include_blank:  false,
              label:          t('.currency_descriptor'),
              hint:           t('.currency_descriptor_hint')
      f.input :emblem,
              as:             :file,
              label:          t('.currency_emblem'),
              hint:           f.object.emblem.present? ? f.template.image_tag(f.object.emblem.url) : f.template.content_tag(:span, t('.currency_emblem_hint'))
      f.input :emblem_cache,
              as:             :hidden
      f.input :display,
              as:             :boolean,
              label:          t('.currency_display')
    end

    f.actions
  end

  index do
    column t('.currency_name') do |c|
      b c.name
    end
    column t('.currency_abbreviation'), :abbreviation
    column t('.currency_emblem') do |c|
      image_tag c.emblem.url
    end
    column t('.currency_ratio') do |c|
      b c.ratio
    end
    column t('.currency_descriptor') do |c|
      status_tag  (c.prime_descriptor? || c.sec_descriptor? ? ( c.prime_descriptor? ? t('.currency_descriptor_prime') : t('.currency_descriptor_sec') ) : t('.currency_no_descriptor')),
                  (c.prime_descriptor? || c.sec_descriptor? ? :ok : :error )
    end
    column t('.currency_display') do |c|
      status_tag  (c.display? ? t('.currency_display_yes') : t('.currency_display_no') ),
                  (c.display? ? :ok : :error )
    end

    default_actions
  end

end
