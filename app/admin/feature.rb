ActiveAdmin.register Feature do

  # Pagination index page
  config.per_page = 20

  # Menu display index
  menu  priority: 5,
        label:    'Технологии',
        parent:   'Меню товаров'

  # Cancel actions
  actions :all, except: [:show]

  # Permission
  permit_params :name, :label, :description

  # Filters
  filter  :name,        label: 'Название технологии:'
  filter  :created_at,  label: 'Дата создания:'

  # New/Edit forms
  form do |f|
    f.inputs t('.feature_form_title') do
      f.input :name,
              required:       true,
              input_html:     { maxlength: 50 },
              label:          t('.feature_name'),
              hint:           t('.feature_name_hint')
      f.input :description,
              as:             :text,
              input_html:     { :rows => 10, :cols => 20, :maxlength => 300 },
              label:          t('.feature_description'),
              hint:           t('.feature_description_hint')
      f.input :label,
              as:             :file,
              label:          t('.feature_image'),
              hint:           f.object.label.present? ? f.template.image_tag(f.object.label.url) : f.template.content_tag(:span, t('.feature_image_hint'))
      f.input :label_cache,
              :as => :hidden
    end

    f.actions
  end

  # Index page
  index do
    selectable_column
    column t('.feature_image'), sortable: false do |f|
      image_tag f.label.url
    end
    column t('.feature_name') do |f|
      b f.name
    end
    column t('.feature_description'), :description

    default_actions
  end
end
