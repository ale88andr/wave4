class Store::Visitors::ApplicationController < ApplicationController
  before_filter :authenticate_user!
end