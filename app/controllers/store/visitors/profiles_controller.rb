class Store::Visitors::ProfilesController < Store::Visitors::ApplicationController

  def new
    @profile = Profile.new(user_id: params[:user_id])
  end

  def create
    @profile = current_user.create_profile(profile_params)
    if @profile.save
      redirect_to root_url, notice: 'Your profile updated!'
    else
      flash[:error] = 'Error updating profile!' and render :new
    end
  end

  def show
    @profile = User.find(params[:user_id])
  end

  def edit
    @profile = current_user.profile
  end

  protected

    def profile_params
      params.require(:profile).permit!
    end
end