class Store::Products::CategoriesController < ApplicationController
  def index
    @categories = Category.for_index
  end

  def show
    category = Category.find(params[:id])
    @category_decorator = CategoryDecorator.new(category)
  end
end
