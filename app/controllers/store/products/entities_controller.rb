# rails g controller Entities index show --no-helper --no-assets --no-view-specs
class Store::Products::EntitiesController < ApplicationController
  def show
    entity = Entity.find(params[:id])
    entity.increment! :views
    @presenter = Entities::ShowPresenter.new(entity)
  end
end