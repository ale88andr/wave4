# rails g controller cart_items --no-helper --no-assets --no-view-specs
class Store::Orders::CartItemsController < ApplicationController
  include CurrentCart

  before_action :set_cart, only: [:create]

  def create
    product = Entity.find(params[:entity_id])
    @cart_item = @cart.add_product(product.id)
    respond_to do |format|
      if @cart_item.save
        format.html { redirect_to store_cart_url(@cart_item.cart), notice: 'Product was successfully added to cart.' }
      else
        format.html { render action: 'new' }
      end
    end
  end

  def destroy
    @cart_item = CartItem.find(params[:id])
    @cart_item.destroy
    respond_to do |format|
      format.html { redirect_to store_cart_url(session[:cart_id]), notice: 'Товарная позиция удаленна!' }
    end
  end

  def cart_item_params
    params.require(:cart_item).permit(:entity_id)
  end
end
