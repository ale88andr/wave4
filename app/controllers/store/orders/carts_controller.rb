# rails g controller carts --no-helper --no-assets --no-view-specs
class Store::Orders::CartsController < ApplicationController

  before_filter only: [:destroy, :show, :confirmation, :create_confirmation] { |m| m.get_cart_by_id(params[:id]) }

  rescue_from ActiveRecord::RecordNotFound, with: :null_cart

  def show
    render 'empty' if @cart.try(:cart_items).blank?
  end

  def destroy
    @cart.destroy if @cart.id == session[:cart_id]
    session[:cart_id] = nil
    respond_to do |format|
      format.html { redirect_to root_url, notice: t('.cart_clear') }
    end
  end

  def empty
  end

  def confirmation
  end

  def create_confirmation
    @cart.user_confirmation = true if @cart
    if @cart.update_attributes(cart_params)
      session[:cart_id] = nil
      redirect_to root_url, notice: t('.sucess_confirmation')
    else
      flash.now[:error] = t('.invalid_cart_form')
      render :confirmation
    end
  end

  protected

    def null_cart
      logger.error t('.logger_error_access_cart', id: params[:id])
      redirect_to store_categories_url, notice: t('.no_cart_isset')
    end

    def get_cart_by_id id
      @cart = Cart.find id
    end

    def cart_params
      params.require(:cart).permit(cart_info: [:username, :street, :note, :city, :phone])
    end
end
