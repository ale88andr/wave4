class ApplicationController < ActionController::Base

  protect_from_forgery with: :exception

  layout :layout_by_resource

  before_filter :configure_permitted_parameters, if: :devise_controller?

  protected

  def layout_by_resource
    devise_controller? ? 'devise' : 'cosmo'
  end

  def configure_permitted_parameters
    devise_parameter_sanitizer.for(:sign_up) { |u| u.permit(:name, :email, :password, :password_confirmation) }
    devise_parameter_sanitizer.for(:sign_in) { |u| u.permit(:name, :email, :password, :remember_me) }
  end

  # redirect after sign out
  def after_sign_out_path_for(resource_or_scope)
    request.referrer
  end

  # redirect after sign up to user profile
  def after_sign_up_path_for(resource)
    new_profile_path
  end

end
